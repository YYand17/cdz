package com.tib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class TIBApplication {

    public static void main(String[] args) {
        SpringApplication.run(TIBApplication.class, args);
    }

}