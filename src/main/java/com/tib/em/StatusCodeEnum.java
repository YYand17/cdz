package com.tib.em;

/*
* 状态码枚举类
* */
public enum StatusCodeEnum {

    SUCCESS(200, "成功"),
    FAIL(-1,"失败"),

    PARAM_MISSING(1001, "参数缺失"),
    DATA_VALIDATION_ERROR(1002, "数据校验错误"),
    INVALID_REQUEST(1003, "无效请求"),
    DUPLICATED_DATA(1004, "重复数据"),

    UNKNOW_ERROR(5000, "未知错误"),
    RESOURCE_MISSING(5001, "未找到该资源"),
    SERVER_INTERNAL_ERROR(5002, "服务器内部错误"),
    SERVER_ARE_TOO_BUSY(5003, "服务器正忙，请稍后再试"),
    REQUEST_TIMEOUT(5004, "请求超时"),
    SERVER_ERROR(5005,"服务器错误"),

    CUSTOM_ERROR(9999, "自定义错误");


    private int code;
    private String desc;

    StatusCodeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return this.code;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "StatusCodeEnum{" + "code='" + code + '\'' + ", desc='" + desc + '\'' + '}';
    }
}
