package com.tib.utils;

import java.util.Map;

/**
 * 用于解析客户端请求json转换的类。
 *
 * @author caoxianghua
 * @create 2021-12-22 14:25
 **/
public class JsonRequest<T> implements Cloneable {

    // 数据
    Object data;

    // data选择的加密方式,MD5,RSA,AES
    String encrypt = "md5";

    // 如果选择了安全方式是md5则对data数据的md5签名，否则是base64Encode(加密算法(data))后的md5签名
    String sign = "";

    long timestamp;

    public JsonRequest(){}

    public JsonRequest(Object data, String encrypt, String sign, long timestamp) {
        super();
        this.data = data;
        this.encrypt = encrypt;
        this.sign = sign;
        this.timestamp = timestamp;
    }

    /**
     * 取得请求参数，以数字返回，即 data 节点下的属性值。
     *
     * @param name 属性值
     * @return 如果找不到的话，返回 null
     */
    public Integer getParamAsInteger(String name) {
        String val = this.getParamAsString(name);
        if (val == null) {
            return null;
        }
        Integer newVal = null;
        try {
            newVal = Integer.valueOf(val);
        } catch (NumberFormatException e) {
            return null;
        }
        return newVal;
    }

    public Long getParamAsLong(String name) {
        String val = this.getParamAsString(name);
        if (val == null) {
            return null;
        }
        Long newVal = null;
        try {
            newVal = Long.valueOf(val);
        } catch (NumberFormatException e) {
            return null;
        }
        return newVal;
    }

    public Float getParamAsFloat(String name) {
        String val = this.getParamAsString(name);
        if (val == null) {
            return null;
        }
        Float newVal = null;
        try {
            newVal = Float.valueOf(val);
        } catch (NumberFormatException e) {
            return null;
        }
        return newVal;
    }

    public Double getParamAsDouble(String name) {
        String val = this.getParamAsString(name);
        if (val == null) {
            return null;
        }
        Double newVal = null;
        try {
            newVal = Double.valueOf(val);
        } catch (NumberFormatException e) {
            return null;
        }
        return newVal;
    }

    public Integer getParamAsInteger(String name, int defaultValue) {
        Integer v = this.getParamAsInteger(name);
        if (v != null) {
            return v;
        }
        return defaultValue;
    }

    /**
     * 取得请求参数，以字符串返回，即 data 节点下的属性值。
     *
     * @param name 属性值
     * @return 如果找不到的话，返回 null
     */
    public String getParamAsString(String name) {
        Object val = this.getParam(name);
        if (val == null) {
            return null;
        }
        return val.toString();
    }

    /**
     * 取得请求参数，即 data 节点下的属性值。
     *
     * @param name 属性值
     * @return 如果找不到的话，返回 null
     */
    @SuppressWarnings("rawtypes")
    public Object getParam(String name) {
        if (this.data == null || name == null) {
            return null;
        }
        if (this.data instanceof Map) {
            Map params = (Map) this.data;
            return params.get(name);
        }
        return null;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getSign() {
        return this.sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getEncrypt() {
        return this.encrypt;
    }

    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "JsonRequest [data=" + data + ", encrypt=" + encrypt + ", sign=" + sign + ", timestamp=" + timestamp
                + "]";
    }

}
