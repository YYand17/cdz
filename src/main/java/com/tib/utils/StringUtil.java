package com.tib.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author caoxianghua
 * @create 2021-12-22 14:55
 **/
public class StringUtil {

    /** 字符长度 {@value} */
    public static final int CHAR_LEN = 1;

    /** 字节长度 {@value} */
    public static final int BYTE_LEN = 2;

    /** 定义一个错误的int类型 {@value} */
    public final static int ERR_INT = -999999999;

    /**
     *
     * @param str 判断自否是否不为空
     * @return 输入字符串
     * @author liuzhengyong date 2018年1月30日
     */
    public static boolean isEmpty(String str) {
        return (str == null) || (str.trim().length() <= 0);
    }

    /**
     * 判断自否是否为空
     *
     * @param str 输入字符串
     * @return
     * @author liuzhengyong date 2018年1月30日
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 判断字符串是否是整数
     *
     * @param str String 字符串
     * @return boolean 是否是整数
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);

        if (!isNum.matches()) {
            return false;
        }

        return true;
    }

    /***
     * 数字字符串转化为整数
     *
     * @param intStr       String 待转化的数字字符串
     * @param defaultValue int 当intStr为空或空字符串时返回的缺省值
     * @return int 返回由数字字符串转化成的数字,当intStr为空或空字符串时,返回缺省值defaultValue
     */
    public static int getInt(String intStr, int defaultValue) {
        if (isEmpty(intStr)) {
            return defaultValue;
        } else {
            int intReturn = defaultValue;
            Double db = new Double(intStr);
            intReturn = db.intValue();

            return intReturn;
        }
    }

    /***
     * 数字字符串转化为整数
     *
     * @param intStr String 待转化的数字字符串
     * @return int 返回由数字字符串转化成的整数,当intStr为空或空字符串时返回0
     */
    public static int getInt(String intStr) {
        return getInt(intStr, 0);
    }

    /***
     * 数字字符串转化为双精数字
     *
     * @param dbstr        String 待转化的数字字符串
     * @param defaultValue double 当dbstr为空或空字符串时返回的缺省值
     * @return double 返回由数字字符串转化成的双精度数字,当dbstr为空或空字符串时,则返回缺省值defaultValue
     */
    public static double getDouble(String dbstr, double defaultValue) {
        if (isEmpty(dbstr)) {
            return defaultValue;
        } else {
            double dbRetu = defaultValue;
            Double db = new Double(dbstr);
            dbRetu = db.doubleValue();

            return dbRetu;
        }
    }

    /***
     * 数字字符串转化为双精度数字
     *
     * @param dbstr String 待转化的数字字符串
     * @return double 返回由数字字符串转化成的双精度数字,当dbstr为空或空字符串时返回0
     */
    public static double getDouble(String dbstr) {
        return getDouble(dbstr, 0.0D);
    }

    /***
     * 数字字符串转化为长整型
     *
     * @param longstr      String 待转化的数字字符串
     * @param defaultValue long 当longstr为空或空字符串时返回的缺省值
     * @return long 返回由数字字符串转化成的长整型数字,当longstr为空或空字符串时,则返回缺省值defaultValue
     */
    public static long getLong(String longstr, long defaultValue) {
        if (isEmpty(longstr)) {
            return defaultValue;
        } else {
            long longRetu = defaultValue;
            Double db = new Double(longstr);
            longRetu = db.longValue();

            return longRetu;
        }
    }

    /***
     * 数字字符串转化为长整型
     *
     * @param longstr String 待转化的数字字符串
     * @return long 返回由数字字符串转化成的长整型数字,当longstr为空或空字符串时返回0
     */
    public static long getLong(String longstr) {
        return getLong(longstr, 0L);
    }

    /***
     * 字符串转化为布尔型
     *
     * @param booleanstr   String 待转化的字符串
     * @param defaultValue boolean 当字符串为空或为空字符串时时返回的值
     * @return boolean 返回转化后的布尔值,只有当booleanstr为字符串 'true' (忽略大小写)或者 '1' 时才返回true,
     *         当字符串为空或为空字符串时,返回缺省值defaultValue
     */
    public static boolean getBoolean(String booleanstr, boolean defaultValue) {
        if (isEmpty(booleanstr)) {
            return defaultValue;
        }

        boolean booleanRetu = defaultValue;
        booleanstr = booleanstr.trim();
        if ("true".equalsIgnoreCase(booleanstr) || equals("1", booleanstr)) {
            booleanRetu = true;
        }

        return booleanRetu;
    }

    /**
     * 两字符串是否相等,同为null也相等
     *
     * @param str1 String 第一个比较字符串
     * @param str2 String 第二个比较字符串
     * @return boolean
     */
    public static boolean equals(String str1, String str2) {
        boolean returnBol = false;

        if (isEmpty(str1) && isEmpty(str2)) {
            returnBol = true;
        }

        if (isNotEmpty(str1) && isNotEmpty(str2)) {
            if (str1.equals(str2)) {
                returnBol = true;
            }
        }

        return returnBol;
    }

    /***
     * 字符串转化为布尔型
     *
     * @param booleanstr String 待转化的字符串
     * @return boolean 返回转化化的布尔值,只有当booleanstr为字符串 'true' (忽略大小写)或者 '1' 时才返回true
     */
    public static boolean getBoolean(String booleanstr) {
        return getBoolean(booleanstr, false);
    }

    /***
     * 双精数字格式化,返回字符串
     *
     * @param db  double 待格式化的双精数字
     * @param fmt String 格式化样式(例如'0.00'),fmt为空、空字符串和非法时,db将按原样转化为字符串后返回
     * @return String 格式化后的数字,以字符串方式返回
     */
    public static String getNumFormat(double db, String fmt) {
        String strReturn = "";

        if (isEmpty(fmt)) {
            return Double.toString(db);
        }

        try {
            DecimalFormat decimalformat = new DecimalFormat(fmt);
            strReturn = decimalformat.format(db);
            decimalformat = null;
        } catch (IllegalArgumentException iaex) {
            strReturn = Double.toString(db);
        }

        return strReturn;
    }

    /***
     * 双精数字格式化,把入参中的双精数格式化为带两位小数的数字字符串
     *
     * @param db double 待格式化的双精数字
     * @return String 格式化为两位小数的数字字符串
     */
    public static String getNumFormat(double db) {
        return getNumFormat(db, "0.00");
    }

    /***
     * 数字字符串格式化,返回字符串
     *
     * @param numStr String 待格式化的数字字符串,如果numStr参数不是数字则不进行格式化,按原样返回
     * @param fmt    String 格式化样式,fmt为空、空字符串和非法格式时,将按numStr原样返回
     * @return String 格式化后的字符串
     */
    public static String getNumFormat(String numStr, String fmt) {
        double db = getDouble(numStr, 0.0D);
        return getNumFormat(db, fmt);
    }

    /***
     * 数字字符串格式化,把入参中的数字字符串格式化为带两位小数的数字字符串
     *
     * @param numStr String 待格式化的数字字符串,如果numStr参数不是的数字则不进行格式化,按原样返回
     * @return String 格式化为两位小数的数字字符串
     */
    public static String getNumFormat(String numStr) {
        return getNumFormat(numStr, "0.00");
    }

    /***
     * 字符串替换,把sourceStr字符串中的所有oldStr子串替换为newStr字串
     *
     * @param sourceStr    String 将被替换的字符串,为null时不执行任何替换操作
     * @param oldStr       String 将被替换的子串,为null或为空字符串时不执行替换操作
     * @param newStr       String 将被替换成的子串,为null时不执行替换操作
     * @param isIgnoreCase boolean 是否忽略大小写,true表忽略大小写,false表大小写敏感。
     * @param isFirst      boolean 是否只替换第一次匹配
     * @return String 替换后的字符串
     */
    public static String replaceString(String sourceStr, String oldStr, String newStr, boolean isIgnoreCase,
                                       boolean isFirst) {
        if (sourceStr == null || oldStr == null || oldStr.equals("")) {
            return null;
        }

        String str_RetStr = sourceStr;
        for (int pos = str_RetStr.indexOf(oldStr); pos != -1; pos = str_RetStr.indexOf(oldStr, pos + newStr.length())) {
            str_RetStr = str_RetStr.substring(0, pos) + newStr + str_RetStr.substring(pos + oldStr.length());
            if (isFirst)
                break;
        }

        return str_RetStr;
    }

    /***
     * 字符串替换,把sourceStr字符串中的所有oldStr子串替换为newStr字串
     *
     * @param sourceStr    String 将被替换的字符串,为null时不执行任何替换操作
     * @param oldStr       String 将被替换的子串,为null或为空字符串时不执行替换操作
     * @param newStr       String 将被替换成的子串,为null时不执行替换操作
     * @param isIgnoreCase boolean 是否忽略大小写,true表忽略大小写,false表大小写敏感。
     * @return String 替换后的字符串
     */
    public static String replaceString(String sourceStr, String oldStr, String newStr, boolean isIgnoreCase) {
        return replaceString(sourceStr, oldStr, newStr, isIgnoreCase, false);
    }

    /***
     * 字符串替换,把sourceStr字符串中的所有oldStr子串替换为newStr字串(大小写敏感)
     *
     * @param sourceStr String 将被替换的字符串,为null时不执行任何替换操作
     * @param oldStr    String 将被替换的子串,为null或为空字符串时不执行替换操作
     * @param newStr    String 将被替换成的子串,为null时不执行替换操作
     * @return String 替换后的字符串
     */
    public static String replaceString(String sourceStr, String oldStr, String newStr) {
        return replaceString(sourceStr, oldStr, newStr, false);
    }

    public static String[] splitString(String sourceStr) {
        String[] rtnStrs = (String[]) null;
        String[] defaultSplitStr = { "|", ",", "，" };
        for (int i = 0; i < defaultSplitStr.length; i++) {
            if (sourceStr.indexOf(defaultSplitStr[i]) >= 0) {
                rtnStrs = splitString(sourceStr, defaultSplitStr[i]);
            }
        }
        if (rtnStrs == null) {
            rtnStrs = new String[] { sourceStr };
        }
        return rtnStrs;
    }

    public static String[] splitString(String sourceStr, String splitStr) {
        if ((isEmpty(sourceStr)) || (isEmpty(splitStr))) {
            return null;
        }

        int int_ArraySize = subStringCount(sourceStr, splitStr);
        if (int_ArraySize == -1) {
            return null;
        }

        sourceStr = sourceStr + splitStr;
        String[] str_RetArr = new String[int_ArraySize + 1];
        int int_pos = sourceStr.indexOf(splitStr);
        int int_ArrayPos = 0;
        for (; int_pos != -1; int_pos = sourceStr.indexOf(splitStr)) {
            str_RetArr[(int_ArrayPos++)] = sourceStr.substring(0, int_pos);
            sourceStr = sourceStr.substring(int_pos + splitStr.length());
        }

        return str_RetArr;
    }

    public static int subStringCount(String sourceStr, String subStr) {
        if ((isEmpty(sourceStr)) || (isEmpty(subStr))) {
            return -1;
        }

        int result = 0;

        for (int int_pos = sourceStr.toUpperCase().indexOf(subStr.toUpperCase()); int_pos != -1;) {
            result++;
            int_pos = sourceStr.toUpperCase().indexOf(subStr.toUpperCase(), int_pos + subStr.length());
        }
        return result;
    }

    /**
     * 获取32位唯一标示符guid
     *
     * @return String 32位唯一标示符guid
     */
    public static String getGuid32() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /***
     * 将字符串填充到指定的长度
     *
     * @param sourceStr  String 源字符串
     * @param length     int 指定长度
     * @param withChar   char 填充的字符
     * @param isPadToEnd boolean 是否填充在字符的尾部 true ：是 ,false:填充在头部
     * @return String
     */
    public static String pad(String sourceStr, int length, char withChar, boolean isPadToEnd) {
        if (sourceStr == null)
            return null;
        if (sourceStr.length() >= length)
            return sourceStr;
        StringBuffer sb = new StringBuffer(sourceStr);
        for (int i = 0; i < length - sourceStr.length(); i++)
            if (isPadToEnd)
                sb.append(withChar);
            else
                sb.insert(0, withChar);

        return sb.toString();
    }

    /**
     * 用指定的分隔符把字符串集合使用分隔符","合并成一个字符串. 数组为null或长度为0时返回空字符串
     *
     * @param strList List<String> 待合并的字符串集合
     * @return String 合并后的字符串
     */
    public static String listToString(List<String> strList) {
        return listToString(strList, ",");
    }

    /**
     * 用指定的分隔符把字符串集合合并成一个字符串. 数组为null或长度为0时返回空字符串
     *
     * @param strList  List<String> 待合并的字符串集合
     * @param splitStr String 数组各元素合并后,它们之间的分隔符,为null时用空字符串""代替
     * @return String 合并后的字符串
     */
    public static String listToString(List<String> strList, String splitStr) {
        if (strList == null || strList.size() == 0) {
            return "";
        }

        if (isEmpty(splitStr)) {
            splitStr = "";
        }

        StringBuffer strBuf = new StringBuffer("");
        for (int i = 0; i < strList.size() - 1; i++)
            strBuf.append(strList.get(i)).append(splitStr);

        strBuf.append(strList.get(strList.size() - 1));

        return strBuf.toString();
    }

    /***
     * 用指定的分隔符把字符串数组合并成一个字符串. 数组为null或长度为0时返回空字符串
     *
     * @param array    String[] 待合并的数组
     * @param splitStr String 数组各元素合并后,它们之间的分隔符,为null时用空字符串""代替
     * @return String 合并后的字符串
     */
    public static String arrayToString(String array[], String splitStr) {

        if (array == null || array.length == 0) {
            return "";
        }

        if (splitStr == null) {
            splitStr = "";
        }

        StringBuffer strBuf = new StringBuffer("");
        int Len = array.length;
        for (int i = 0; i < Len - 1; i++)
            strBuf.append(array[i] != null ? array[i] : "").append(splitStr);

        strBuf.append(array[Len - 1] != null ? array[Len - 1] : "");
        return strBuf.toString();
    }

    /***
     * 用默认分隔符'|'把字符串数组合并成一个字符串. 数组为null或长度为0时返回空字符串
     *
     * @param array String[] 待合并的数组
     * @return String 合并后的字符串
     */
    public static String arrayToString(String array[]) {
        return arrayToString(array, "|");
    }

    /***
     * 删除指定的前导、后导子串(大小写敏感)
     *
     * @param sourceStr  String 源字符串
     * @param removeChar char 子串
     * @return String 处理后的字符串
     */
    public static String trim(String sourceStr, char removeChar) {
        if (sourceStr == null)
            return null;
        sourceStr = sourceStr.trim();
        int loInt_begin = 0;
        int loInt_end = 0;
        int loInt_len = sourceStr.length();
        for (int i = 0; i < loInt_len; i++) {
            if (sourceStr.charAt(i) != removeChar)
                break;
            loInt_begin++;
        }

        for (int i = 0; i < loInt_len; i++) {
            if (sourceStr.charAt(loInt_len - 1 - i) != removeChar)
                break;
            loInt_end++;
        }

        return sourceStr.substring(loInt_begin, loInt_len - loInt_end);
    }

    /***
     * 删除指定的前导、后导子串(大小写敏感)
     *
     * @param sourceStr  String 源字符串
     * @param removeChar char 子串
     * @param end        true 只trim末尾,false 只trim头部
     * @return String 处理后的字符串
     */
    public static String trim(String sourceStr, char removeChar, boolean end) {
        if (sourceStr == null)
            return null;
        sourceStr = sourceStr.trim();
        int loInt_begin = 0;
        int loInt_end = 0;
        int loInt_len = sourceStr.length();
        for (int i = 0; i < loInt_len; i++) {
            if (sourceStr.charAt(i) != removeChar)
                break;
            loInt_begin++;
        }

        for (int i = 0; i < loInt_len; i++) {
            if (sourceStr.charAt(loInt_len - 1 - i) != removeChar)
                break;
            loInt_end++;
        }

        if (end)
            return sourceStr.substring(0, loInt_len - loInt_end);
        else
            return sourceStr.substring(loInt_begin);
    }

    /***
     * 将字符串截短到特定的长度,一个中文是两个字节的长度,但是是1个字符的长度
     *
     * @param str     String 需要截的字符串
     * @param len     int 截到的目标长度
     * @param lenType int 长度类型,字符长度为常量CHAR_LEN,字节长度为常量BYTE_LEN
     * @return String
     */
    public static String truncate(String str, int len, int lenType) {
        if (str == null)
            return str;
        if (lenType == 1)
            if (str.length() <= len)
                return str;
            else
                return str.substring(0, len);
        StringBuffer sb = new StringBuffer();
        int temp = 0;
        for (int i = 0; i < str.length(); i++) {
            if (i >= len)
                break;

            char ctemp = str.charAt(i);
            String stemp = ctemp + "";
            temp = stemp.getBytes().length;
            if (i + temp > len)
                break;
            sb.append(str.charAt(i));
        }

        return sb.toString();
    }

    /***
     * 从源字符串中从第一个字符开始取出给定长度的字串. 源字符串长度大于len时,尾巴追加一个appendStr串
     *
     * @param sourceStr String 源字符串
     * @param len       int 取出的长度
     * @param appendStr String 追加的字符串(常用的appendStr为...)
     * @return String 取出的子串
     */
    public static String substring(String sourceStr, int len, String appendStr) {
        if (sourceStr == null || "".equals(sourceStr))
            return sourceStr;
        if (len <= 0)
            return "";
        if (appendStr == null)
            appendStr = "";
        int sourceLen = sourceStr.length();
        if (len >= sourceLen)
            return sourceStr;
        else
            return sourceStr.substring(0, len) + appendStr;
    }

    /***
     * 产生纯数字随机字符串
     *
     * @param length int 随机字符串长度
     * @return String 随机字符串
     */
    public static String randomNumber(int length) {
        String retu = "";

        for (int i = 0; i < length; i++)
            retu = retu + (int) (Math.random() * 10D);

        return retu;
    }

    /**
     * 字符串从原编码转成 GB2312 编码<br>
     * <strong>示例:</strong><br>
     * 描述:将字符串从指定的编码转为GB2312 编码<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;StringUtil.getGB("我们", "UTF-8");<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;返回的结果是:??滑<br>
     *
     * @param str          String 输入字符串
     * @param sourceEncode String 字符串原编码
     * @return String 编码转换后的字符串
     */
    public static String getGB(String str, String sourceEncode) {
        if (isEmpty(sourceEncode)) {
            return str;
        }

        String strReturn = "";

        try {
            strReturn = new String(str.getBytes(sourceEncode), "gb2312");
        } catch (Exception ex) {
        }

        return strReturn;
    }

    /**
     * 字符串从 ISO-8859-1 编码转成 GB2312 编码<br>
     * <strong>示例:</strong><br>
     * &nbsp;&nbsp;&nbsp;&nbsp;StringUtil.getGB("我们");<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;返回的结果是:??<br>
     *
     * @param str String 输入字符串
     * @return String 编码转换后的字符串
     */
    public static String getGB(String str) {
        return getGB(str, "iso-8859-1");
    }

    /**
     * 字符串编码的转换
     *
     * @param str    String 输入字符串
     * @param encode String 源编码
     * @param decode String 目标编码
     * @return String 编码转换后的字符串
     */
    public static String getTranscoding(String str, String encode, String decode) {

        String strReturn = "";

        try {
            strReturn = new String(str.getBytes(encode), decode);
        } catch (Exception ex) {

        }

        return strReturn;
    }

    /**
     * 字符串对象去空格,空对象转成空字符,去除字符串中的前后空格
     *
     * @param obj String 字符串对象
     * @return String 去空格后的字符串
     */
    public static String clearNull(String obj) {
        if (obj == null) {
            return "";
        } else {
            return obj.trim();
        }
    }

    /**
     * 清除字符串中的空格
     *
     * @param str String 源字符串
     * @return String
     */
    public static String clearNullChar(String str) {
        return clearChar(str, ' ');
    }

    /**
     * 清除字符串中指定的字符
     *
     * @param str String 源字符串
     * @param c   char 要清除的字符
     * @return String
     */
    public static String clearChar(String str, char c) {
        if (isEmpty(str)) {
            return str;
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch != c) {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    /**
     * 基于natvie2asii命令将字符串转为Unicode编码
     *
     * @param str String 要转换的的字符串
     * @return String String 编码转换后的字符串
     */
    public static String getUnicodeString(String str) {
        StringBuffer tempSb = new StringBuffer();

        try {
            // 加载指定的native2ascii.exe
            Process pro = Runtime.getRuntime().exec("native2ascii");

            // 获取指定的native2ascii.exe的输出流
            OutputStream out = pro.getOutputStream();

            // 通过该输入流写入字符串的字节码,其目的可以转为unicode
            out.write(str.getBytes());
            out.flush();
            out.close();

            // 再获取native2ascii.exe的输入流
            InputStreamReader child_in = new InputStreamReader(pro.getInputStream());

            // 将转换过的数据以字符的格式取出组成一个字符串
            int c;
            while ((c = child_in.read()) != -1) {
                tempSb.append((char) c);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return tempSb.toString();
    }

    /**
     * 基于正则表达式将字符串转为Unicode编码([a-z][A-Z][0-9]’-()*|&#/\@!?,><%’等字符不处理，其它字符进行Unicode编码处理)
     *
     * @param str String 要转换的的字符串
     * @return 字符串
     */
    public static String convertToUnicode(String str) {
        if (isEmpty(str)) {
            return "";
        }

        String unicode = "";
        String regex = "[a-z|A-Z|0-9|\\-|\\(|\\)|\\*|\\||\\&|\\#|\\/|\\\\|\\@|\\!|\\?|\\,|\\>|\\<|\\%]";
        // String regex = "[a-z|A-Z|0-9]";

        Pattern p = Pattern.compile(regex);

        char[] charAry = new char[str.length()];

        for (int i = 0; i < charAry.length; i++) {
            charAry[i] = (char) str.charAt(i);
            Matcher m = p.matcher(charAry[i] + "");
            if (m.find()) {
                unicode += charAry[i];
            } else {
                unicode += "\\u" + Integer.toString(charAry[i], 16);
            }
        }

        return unicode;
    }

    /**
     * 按指定分隔符将字符串分割为字符串数组
     *
     * @param strValue String 字符串
     * @param boffex   String 分隔符
     * @return String[]
     */
    public static String[] getStringArray(String strValue, String boffex) {
        String strValuearr[] = (String[]) null;

        StringTokenizer strToken = new StringTokenizer(strValue, boffex);
        Vector<Object> vecStrValue = new Vector<Object>();
        for (; strToken.hasMoreElements(); vecStrValue.addElement(strToken.nextToken()))
            ;
        strValuearr = new String[vecStrValue.size()];
        vecStrValue.copyInto(strValuearr);

        return strValuearr;
    }

    /**
     * 将字符串数组转换为整形数组
     *
     * @param strValues String 字符串数组
     * @return int[]
     */
    public static int[] getIntArray(String strValues[]) {
        int iValues[] = (int[]) null;
        iValues = new int[strValues.length];
        for (int i = 0; i < strValues.length; i++)
            iValues[i] = Integer.parseInt(strValues[i].trim());

        return iValues;
    }

    /***
     * 字符串UTF-8编码
     *
     * @param str String 要编码的字符串
     * @return String UTF-8编码后的字符串
     */
    public static String toUtf8String(String str) {

        String strTitle = str.substring(0, str.lastIndexOf("."));
        String strExct = str.substring(str.lastIndexOf("."), str.length());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < strTitle.length(); i++) {
            char c = strTitle.charAt(i);
            if (c >= 0 && c <= '\377') {
                if (sb.toString().length() < 147) {
                    sb.append(c);
                    continue;
                }
                sb.append("[1]");
                break;
            }
            byte b[];
            try {
                b = (new Character(c)).toString().getBytes("UTF-8");
            } catch (Exception ex) {
                System.out.println(ex);
                b = new byte[0];
            }
            if (sb.toString().length() < 140) {
                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0)
                        k += 256;
                    sb.append("%" + Integer.toHexString(k).toUpperCase());
                }

                continue;
            }
            sb.append("[1]");
            break;
        }

        return sb.toString() + strExct;
    }

    /**
     * 判断字符串数组中是否包含某字符
     *
     * @param strArray String[] 字符串数组
     * @param str      String 字符串
     * @return boolean true表示包含,false表示不包含
     */
    public static boolean haveStrInStringArray(String[] strArray, String str) {
        return contains(strArray, str);
    }

    /**
     * 判断字符串数组中是否包含某字符
     *
     * @param strArray String[] 字符串数组
     * @param str      String 字符串
     * @return boolean true表示包含,false表示不包含
     */
    public static boolean haveStrInStringArray(String[] strArray, String str, boolean equalsIgnoreCase) {
        return contains(strArray, str, equalsIgnoreCase);
    }

    /**
     * 判断字符串数组中是否包含某字符,同haveStrInStringArray方法
     *
     * @param strArray String[]字符串数组
     * @param str      String 字符串
     * @return boolean true表示包含,false表示不包含
     */
    public static boolean contains(String[] strArray, String str) {
        return contains(strArray, str, true);
    }

    /**
     * 判断字符串数组中是否包含某字符,同haveStrInStringArray方法
     *
     * @param strArray         String[]字符串数组
     * @param str              String 字符串
     * @param equalsIgnoreCase boolean 是否区分大小写
     * @return boolean true表示包含,false表示不包含
     */
    public static boolean contains(String[] strArray, String str, boolean equalsIgnoreCase) {
        if (strArray == null || strArray.length == 0 || isEmpty(str)) {
            return false;
        } else {
            for (int i = 0; i < strArray.length; i++) {
                if (equalsIgnoreCase) {
                    if (strArray[i].equals(str)) {
                        return true;
                    }
                } else {
                    if (strArray[i].equalsIgnoreCase(str)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * 计算字符串长度,一个汉字的长度按2计算.如果给定的字符串为null,返回0.
     *
     * @param str String 要计算的字符串
     * @return int
     */
    public static int strlen(String str) {
        if (str == null || str.length() <= 0) {
            return 0;
        }

        int len = 0;
        char c;
        for (int i = str.length() - 1; i >= 0; i--) {
            c = str.charAt(i);
            if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                // 字母, 数字
                len++;
            } else {
                int ascii = Character.codePointAt(str, i);
                if (ascii >= 0 && ascii <= 255) {
                    len++;
                } else {
                    len += 2;
                }
            }
        }

        return len;
    }

    /***
     * 把字符串的首字符转化为大写或者小写
     *
     * @param isUpperChar boolean 是否是转化为大写标志
     * @param src         String 需要转化的数据
     * @return String 转化好的数据
     */
    public static String transFirstChar(boolean isUpperChar, String src) {
        StringBuffer sb = new StringBuffer("");
        if (isUpperChar)
            sb.append(src.substring(0, 1).toUpperCase());
        else
            sb.append(src.substring(0, 1).toLowerCase());
        sb.append(src.substring(1));

        return sb.toString();
    }

    /**
     * 数字转成大写
     *
     * @param x
     * @return
     */
    public static String formatDateC(char x) {
        String[] rmbnum = { "〇", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
        char[] srcChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        for (int i = 0; i < srcChar.length; i++) {
            if (srcChar[i] == x)
                return rmbnum[i];
        }
        return rmbnum[1];
    }

    /**
     * 获取类某个属性的整型编码
     *
     * @param fieldName String 字段名称, 必须是整型数据类型
     * @param class1    Class
     * @return int
     */
    public static int getTypeCode(String fieldName, Class<?> class1) {
        Field[] fields = class1.getDeclaredFields();
        int fieldNum = fields.length;
        for (int i = 0; i < fieldNum; i++) {
            if (fields[i].getName().equals(fieldName))
                try {
                    return getInt(fields[i].get(null).toString());

                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                } catch (IllegalArgumentException ex) {
                    ex.printStackTrace();
                }
        }

        return StringUtil.ERR_INT;
    }

    public static String formatB(char x) {
        String[] rmbnum = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
        char[] srcChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        for (int i = 0; i < srcChar.length; i++) {
            if (srcChar[i] == x)
                return rmbnum[i];
        }
        return rmbnum[1];
    }

    public static String formatC(char x) {
        String[] rmbnum = { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };
        char[] srcChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        for (int i = 0; i < srcChar.length; i++) {
            if (srcChar[i] == x)
                return rmbnum[i];
        }
        return rmbnum[1];
    }

    public static String convertIntPartNum(String str) {
        int tempIdx = 0;
        for (int i = 0; i < str.length(); i++) {
            char c1 = str.charAt(i);
            if (c1 != '0') {
                tempIdx = i;
                break;
            }
        }
        str = str.substring(tempIdx);
        int strLen = str.length();
        int n = strLen / 4;
        int m = strLen % 4;
        if (m > 0) {
            for (int i = 4 - m; i > 0; i--) {
                str = '0' + str;
            }
            n++;
        }
        while (str.length() < 4) {
            str = '0' + str;
        }
        String eachSplitStr = "";
        String change = "";
        char[] array = new char[4];
        char[] array2 = { '千', '百', '十' };
        char[] array3 = { '亿', '万', ' ', ' ' };
        String outStr = "";
        for (int i = 0; i < n; i++) {
            eachSplitStr = "";
            change = str.substring(4 * i, 4 * (i + 1));
            array = change.toCharArray();
            for (int j = 0; j < 4; j++) {
                eachSplitStr = eachSplitStr + formatB(array[j]);
                if ((j < 3) && (array[j] != '0')) {
                    eachSplitStr = eachSplitStr + array2[j];
                }
            }
            int q = eachSplitStr.indexOf("零零");
            while (q != -1) {
                eachSplitStr = eachSplitStr.substring(0, q) + "零" + eachSplitStr.substring(q + 2);
                q = eachSplitStr.indexOf("零零");
            }
            if (eachSplitStr.endsWith("零")) {
                eachSplitStr = eachSplitStr.substring(0, eachSplitStr.length() - 1);
            }
            eachSplitStr = eachSplitStr + array3[(3 - (n - i))];
            if ((i != n - 1) && (eachSplitStr.length() == 0))
                outStr = outStr + "零";
            outStr = outStr + eachSplitStr.trim();
        }
        outStr = outStr.replaceAll("零零", "零");
        outStr = outStr.replaceFirst("亿万", "亿");
        outStr = outStr.replaceAll("一十", "十");
        int begInt = 0;
        int endInt = outStr.length();
        tempIdx = endInt;
        for (int i = 0; i < endInt; i++) {
            if (outStr.charAt(i) != 38646) {
                begInt = i;
                break;
            }
        }
        for (int i = tempIdx - 1; i > -1; i++) {
            if (outStr.charAt(i) != 38646) {
                endInt = i;
                break;
            }
        }
        outStr = outStr.substring(begInt, endInt + 1);
        return outStr;
    }

    public static String incision(Object object) {
        String string = object.toString();
        int before = string.indexOf("[");
        int later = string.indexOf("]");
        return string.substring(before + 1, later);
    }

    public static String[] list2StrArr(List<Object> list) {
        String[] strs = new String[list.size()];
        for (int i = 0; i < strs.length; i++) {
            strs[i] = list.get(i).toString();
        }
        return strs;
    }

    public static String getDateByString(String str, String dateFormat){
        Date date = JSONObject.parseObject(JSON.toJSONString(str),Date.class);
        if (dateFormat.isEmpty()){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(date);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }

}
