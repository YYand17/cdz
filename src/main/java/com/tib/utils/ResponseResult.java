package com.tib.utils;

import lombok.Data;

@Data
public class ResponseResult {

    private Integer status;
    private String message;
    private Object object;

    private ResponseResult(Integer status,String message,Object object){
        this.status = status;
        this.message = message;
        this.object = object;
    }

    public static ResponseResult getResponseResult(Integer status,String message,Object object){
        return new ResponseResult(status,message,object);
    }

    public static ResponseResult getResponseResult(Integer status,String message){
        return getResponseResult(status,message,null);
    }

    public static ResponseResult getResponseResult(Integer status){
        return getResponseResult(status,"请求失败",null);
    }

}
