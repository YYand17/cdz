package com.tib.utils;

import lombok.Data;

import java.util.List;

@Data
public class PageResult {
    public static final int DEFAULT_PAGE = 1;
    public static final int DEFAULT_ROW = 5;
    private Long total;// 总条数
    private Long totalPage;// 总页数
    private List items;// 当前页数据

    public PageResult(){}

    public PageResult(Long total, Long totalPage, List items) {
        this.total = total;
        this.totalPage = totalPage;
        this.items = items;
    }
}
