package com.tib.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-09 16:00
 **/

@Mapper
@Repository
public interface LoginMapper {
    Map<String,Object> login(Map<String,Object> map);


}
