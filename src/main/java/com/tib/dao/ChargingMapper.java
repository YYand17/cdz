package com.tib.dao;

import com.tib.pojo.Charging;
import com.tib.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ChargingMapper {

    int insert(Charging record);

    Map select(String id);

    int update(Charging charging);

    List<Map<String,Object>> queryAll(Map map);

    int delete(String id);

    Map selectCount();
}