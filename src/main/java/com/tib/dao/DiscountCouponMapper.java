package com.tib.dao;

import com.tib.pojo.DiscountCoupon;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DiscountCouponMapper {

    List<Map> queryAll(Map map);

    int delete(String id);

    int insert(DiscountCoupon record);

    int update(DiscountCoupon record);

    Map selectCount();
}