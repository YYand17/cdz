package com.tib.dao;

import com.tib.pojo.UserCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserCollectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserCollect record);

    int insertSelective(UserCollect record);

    UserCollect selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserCollect record);

    int updateByPrimaryKey(UserCollect record);

    List<UserCollect> selectByUserId(UserCollect record);

    int selectPageCount(UserCollect record);

    List<UserCollect> selectPage(@Param("userCollect")UserCollect record, @Param("start")int start, @Param("row")int row);
}