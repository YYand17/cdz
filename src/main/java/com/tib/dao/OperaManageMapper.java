package com.tib.dao;
import com.tib.pojo.OperaManage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface OperaManageMapper {
    int delete(String id);

    int insert(Map map);

    List<Map> select(Map map);

    int selectCount(Map map);

    int update(Map map);

    List<Map> getCount(Map map);
}