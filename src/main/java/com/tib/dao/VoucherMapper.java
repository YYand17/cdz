package com.tib.dao;

import com.tib.pojo.Voucher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface VoucherMapper {

    List<Map> queryAll(Map map);

    int delete(String id);

    int insert(Voucher record);

    int update(Voucher record);

    Map selectCount(Map map);
}