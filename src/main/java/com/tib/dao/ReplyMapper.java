package com.tib.dao;

import com.tib.pojo.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ReplyMapper {
    int delete(String id);

    int insert(Reply record);

    int update(Reply record);

    List<Map> select(Map map);

    Map selectCount(Map map);
}