package com.tib.dao;

import com.tib.pojo.Charging;
import com.tib.pojo.RechargeRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface RechargeRecordMapper {

    int delete(String id);

    int insert(RechargeRecord record);

    List<Map> select(Map map);

    Map selectCount(Map map);

    int update(RechargeRecord record);
}