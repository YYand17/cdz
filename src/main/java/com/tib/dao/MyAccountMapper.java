package com.tib.dao;

import com.tib.pojo.Charging;
import com.tib.pojo.MyAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface MyAccountMapper {
    int delete(String id);

    int insert(MyAccount record);

    List<Map> select(Map map);

    Map selectCount(Map map);

    int update(Map map);

    Map<String,Object> selected(Map<String,Object> map);
}