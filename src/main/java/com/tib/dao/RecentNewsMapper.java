package com.tib.dao;

import com.tib.pojo.RecentNews;
import com.tib.pojo.SystemMessages;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface RecentNewsMapper {
    int delete(String id);

    int insert(RecentNews record);

    List<Map> select(Map map);

    int update(Map map);

    int selectCount(Map map);

}