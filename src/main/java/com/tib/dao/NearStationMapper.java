package com.tib.dao;

import com.tib.pojo.NearStation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface NearStationMapper {
    int delete(String id);

    int insert(NearStation record);

    int update(NearStation record);

    List<Map> queryAll(Map map);

    Map selectCount(Map map);
}