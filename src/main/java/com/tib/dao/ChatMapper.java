package com.tib.dao;

import com.tib.pojo.Chat;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ChatMapper {
    int deleteById(String id);

    int insert(Chat record);

    int updateById(Chat record);

    List<Map<String,Object>> findAll(Map map);

    Map selectCount();
}