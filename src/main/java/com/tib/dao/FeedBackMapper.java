package com.tib.dao;

import com.tib.pojo.FeedBack;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface FeedBackMapper {
    int delete(String id);

    int insert(FeedBack record);

    List<Map> select(Map map);

    int update(FeedBack record);

    Map selectCount();
}