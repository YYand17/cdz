package com.tib.dao;

import com.tib.pojo.ManagerSite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ManagerSiteMapper {
    int insert(ManagerSite record);

    int insertSelective(ManagerSite record);

    List<ManagerSite> queryAll();

    ManagerSite select(Integer id);

    int update(ManagerSite managerSite);

    int delete(Integer id);

    List<ManagerSite> selectPage(@Param("managerSite") ManagerSite managerSite, @Param("start") Integer page,@Param("row") Integer row);

    long selectPageCount();

    List<ManagerSite> AssociatedQuery(@Param("userId") Integer userId);
}