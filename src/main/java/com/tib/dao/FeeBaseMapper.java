package com.tib.dao;

import com.tib.pojo.FeeBase;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FeeBaseMapper {

    List<FeeBase> queryAll();

    int deleteByPrimaryKey(Integer id);

    int insert(FeeBase record);

    int insertSelective(FeeBase record);

    FeeBase selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FeeBase record);

    int updateByPrimaryKey(FeeBase record);

    int selectPageCount(FeeBase record);

    List<FeeBase> selectPage(@Param("feeBase")FeeBase record, @Param("start")int start, @Param("row")int row);
}