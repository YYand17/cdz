package com.tib.dao;

import com.tib.pojo.ChargingOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ChargingOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargingOrder record);

    int insertSelective(ChargingOrder record);

    ChargingOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChargingOrder record);

    int updateByPrimaryKey(ChargingOrder record);

    int selectPageCount(ChargingOrder record);

    List<ChargingOrder> selectByBean(ChargingOrder record);

    List<ChargingOrder> selectPage(@Param("chargingOrder") ChargingOrder record, @Param("start") int start, @Param("row") int row);

    List<ChargingOrder> selectLately(@Param("chargingOrder") ChargingOrder record, @Param("start") int start, @Param("row") int row);

}