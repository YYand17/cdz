package com.tib.dao;

import com.tib.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CommentMapper {

    Integer insertSelective(Comment record);

    Map<String,Object> selectByPrimaryKey(Map map);

    List<Map<String,Object>> selectAll(Map map);

    Integer update(Comment comment);

    Integer delete(String id);

    Map<String ,Object> selectCount();

}