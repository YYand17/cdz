package com.tib.dao;

import com.tib.pojo.ChargingOrder;
import com.tib.pojo.SystemMessages;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SystemMessagesMapper {
    int delete(String id);

    int insert(SystemMessages record);

    List<Map> select(Map map);

    int update(SystemMessages record);

    Map selectCount(Map map);
}