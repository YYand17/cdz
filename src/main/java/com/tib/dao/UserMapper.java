package com.tib.dao;

import com.tib.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserMapper {
    //获取所有人的信息
    List<User> queryAll();

    //通过ID获取用户信息
    User queryUserByID(@Param("id") Integer id);

    //<!--    查询username和password-->
    User queryIDAndPassword(User user);

    void insertUserInfo(User user);

    int queryMaxUserID();

    void updateUserInfo(User user);

    void uploadImage(int id,String file);

    List<User> queryPage(@Param("user")User user, @Param("start")int start, @Param("row")int row);

    int selectCount(Map map);

    List<Map> select(Map map);

    int update(Map map);
    //===================================================================

//    int deleteByPrimaryKey(Integer id);
//
//    int insert(User record);
//
//    int insertSelective(User record);
//
//    User selectByPrimaryKey(Integer id);
//
//    int updateByPrimaryKeySelective(User record);
//
//    int updateByPrimaryKeyWithBLOBs(User record);
//
//    int updateByPrimaryKey(User record);
}
