package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("卡号管理表-实体类")
public class FeeBase {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("卡号")
    private String cardNumber;

    @ApiModelProperty("卡号状态")
    private String cardStatus;

    @ApiModelProperty("卡绑定手机号")
    private String phone;

    @ApiModelProperty("申请时间")
    private Date registeredTime;

    @ApiModelProperty("外键，用户id")
    private Integer userId;

    @ApiModelProperty("外键，运营商id")
    private Integer operatorId;

    @ApiModelProperty("实收金额")
    private Long acceptAmount;

    @ApiModelProperty("赠送金额")
    private Long giftAmount;

    @ApiModelProperty("结算类型")
    private String settementType;

    @ApiModelProperty("卡密码")
    private String cardPass;

    @ApiModelProperty("车牌号")
    private String carNumber;

    @ApiModelProperty("车类类型")
    private String carType;

    @ApiModelProperty("是否支持多充")
    private Integer isMore;

    @ApiModelProperty("是否允许负数")
    private Integer isNegative;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber == null ? null : cardNumber.trim();
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus == null ? null : cardStatus.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Long getAcceptAmount() {
        return acceptAmount;
    }

    public void setAcceptAmount(Long acceptAmount) {
        this.acceptAmount = acceptAmount;
    }

    public Long getGiftAmount() {
        return giftAmount;
    }

    public void setGiftAmount(Long giftAmount) {
        this.giftAmount = giftAmount;
    }

    public String getSettementType() {
        return settementType;
    }

    public void setSettementType(String settementType) {
        this.settementType = settementType == null ? null : settementType.trim();
    }

    public String getCardPass() {
        return cardPass;
    }

    public void setCardPass(String cardPass) {
        this.cardPass = cardPass == null ? null : cardPass.trim();
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber == null ? null : carNumber.trim();
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    public Integer getIsMore() {
        return isMore;
    }

    public void setIsMore(Integer isMore) {
        this.isMore = isMore;
    }

    public Integer getIsNegative() {
        return isNegative;
    }

    public void setIsNegative(Integer isNegative) {
        this.isNegative = isNegative;
    }
}