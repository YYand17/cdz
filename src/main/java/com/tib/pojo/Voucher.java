package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("代金券表-实体类")
public class Voucher {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户id")
    private String uId;

    @ApiModelProperty("代金券名称")
    private String vcName;

    @ApiModelProperty("代金券金额")
    private Long vcMoney;

    @ApiModelProperty("代金券期限")
    private Date vcDate;

    @ApiModelProperty("代金券类型")
    private String vcState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getVcName() {
        return vcName;
    }

    public void setVcName(String vcName) {
        this.vcName = vcName;
    }

    public Long getVcMoney() {
        return vcMoney;
    }

    public void setVcMoney(Long vcMoney) {
        this.vcMoney = vcMoney;
    }

    public Date getVcDate() {
        return vcDate;
    }

    public void setVcDate(Date vcDate) {
        this.vcDate = vcDate;
    }

    public String getVcState() {
        return vcState;
    }

    public void setVcState(String vcState) {
        this.vcState = vcState;
    }
}