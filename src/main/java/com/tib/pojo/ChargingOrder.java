package com.tib.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Repository;

import java.util.Date;
@Data
@Repository
@ApiModel("充电订单表-实体类")
public class ChargingOrder {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("充电桩编号")
    private String chargingPileNum;

    @ApiModelProperty("用电类型")
    private String eleType;

    @ApiModelProperty("卡号id")
    private Integer cardId;

    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("开始电量，%")
    private Long startElectricQuantity;

    @ApiModelProperty("结束电量，%")
    private Long endElectricQuantity;

    @ApiModelProperty("用电功率")
    private Long electricPower;

    @ApiModelProperty("总金额")
    private Long sumAmount;

    @ApiModelProperty("用电费用")
    private Long electricAmount;

    @ApiModelProperty("服务费")
    private Long serviceAmount;

    @ApiModelProperty("折扣费")
    private Long accountAmount;

    @ApiModelProperty("说明")
    private String explainBook;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChargingPileNum() {
        return chargingPileNum;
    }

    public void setChargingPileNum(String chargingPileNum) {
        this.chargingPileNum = chargingPileNum == null ? null : chargingPileNum.trim();
    }

    public String getEleType() {
        return eleType;
    }

    public void setEleType(String eleType) {
        this.eleType = eleType == null ? null : eleType.trim();
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getStartElectricQuantity() {
        return startElectricQuantity;
    }

    public void setStartElectricQuantity(Long startElectricQuantity) {
        this.startElectricQuantity = startElectricQuantity;
    }

    public Long getEndElectricQuantity() {
        return endElectricQuantity;
    }

    public void setEndElectricQuantity(Long endElectricQuantity) {
        this.endElectricQuantity = endElectricQuantity;
    }

    public Long getElectricPower() {
        return electricPower;
    }

    public void setElectricPower(Long electricPower) {
        this.electricPower = electricPower;
    }

    public Long getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(Long sumAmount) {
        this.sumAmount = sumAmount;
    }

    public Long getElectricAmount() {
        return electricAmount;
    }

    public void setElectricAmount(Long electricAmount) {
        this.electricAmount = electricAmount;
    }

    public Long getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(Long serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    public Long getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(Long accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getExplainBook() {
        return explainBook;
    }

    public void setExplainBook(String explainBook) {
        this.explainBook = explainBook == null ? null : explainBook.trim();
    }
}