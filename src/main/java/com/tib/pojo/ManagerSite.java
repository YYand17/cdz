package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("站点管理表-实体类")
public class ManagerSite {

    private Integer id;

    private String speratorName;

    private String siteName;

    private String frontIpAddress;

    private String frontPort;

    private String locationType;

    private String registrationSite;

    private String city;

    private String district;

    private String streetAddress;

    private String districtNum;

    private String pileNumber;

    private Integer parkingNum;

    private Long area;

    private String root;

    private String phone;

    private Date time;

    private String servicePhone;

    private String state;

    private String type;

    private String substationBoxType;

    private String photovoltaicInverterType;

    private String electricMeterType;

    private String totalChargingCapacity;

    private String possessionPileCharge;

    private String possessionPileChargeStart;

    private String chargeUnitPrice;

    public String getSperatorName() {
        return speratorName;
    }

    public void setSperatorName(String speratorName) {
        this.speratorName = speratorName == null ? null : speratorName.trim();
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName == null ? null : siteName.trim();
    }

    public String getFrontIpAddress() {
        return frontIpAddress;
    }

    public void setFrontIpAddress(String frontIpAddress) {
        this.frontIpAddress = frontIpAddress == null ? null : frontIpAddress.trim();
    }

    public String getFrontPort() {
        return frontPort;
    }

    public void setFrontPort(String frontPort) {
        this.frontPort = frontPort == null ? null : frontPort.trim();
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType == null ? null : locationType.trim();
    }

    public String getRegistrationSite() {
        return registrationSite;
    }

    public void setRegistrationSite(String registrationSite) {
        this.registrationSite = registrationSite == null ? null : registrationSite.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress == null ? null : streetAddress.trim();
    }

    public String getDistrictNum() {
        return districtNum;
    }

    public void setDistrictNum(String districtNum) {
        this.districtNum = districtNum == null ? null : districtNum.trim();
    }

    public String getPileNumber() {
        return pileNumber;
    }

    public void setPileNumber(String pileNumber) {
        this.pileNumber = pileNumber == null ? null : pileNumber.trim();
    }

    public Integer getParkingNum() {
        return parkingNum;
    }

    public void setParkingNum(Integer parkingNum) {
        this.parkingNum = parkingNum;
    }

    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root == null ? null : root.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone == null ? null : servicePhone.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getSubstationBoxType() {
        return substationBoxType;
    }

    public void setSubstationBoxType(String substationBoxType) {
        this.substationBoxType = substationBoxType == null ? null : substationBoxType.trim();
    }

    public String getPhotovoltaicInverterType() {
        return photovoltaicInverterType;
    }

    public void setPhotovoltaicInverterType(String photovoltaicInverterType) {
        this.photovoltaicInverterType = photovoltaicInverterType == null ? null : photovoltaicInverterType.trim();
    }

    public String getElectricMeterType() {
        return electricMeterType;
    }

    public void setElectricMeterType(String electricMeterType) {
        this.electricMeterType = electricMeterType == null ? null : electricMeterType.trim();
    }

    public String getTotalChargingCapacity() {
        return totalChargingCapacity;
    }

    public void setTotalChargingCapacity(String totalChargingCapacity) {
        this.totalChargingCapacity = totalChargingCapacity == null ? null : totalChargingCapacity.trim();
    }

    public String getPossessionPileCharge() {
        return possessionPileCharge;
    }

    public void setPossessionPileCharge(String possessionPileCharge) {
        this.possessionPileCharge = possessionPileCharge == null ? null : possessionPileCharge.trim();
    }

    public String getPossessionPileChargeStart() {
        return possessionPileChargeStart;
    }

    public void setPossessionPileChargeStart(String possessionPileChargeStart) {
        this.possessionPileChargeStart = possessionPileChargeStart == null ? null : possessionPileChargeStart.trim();
    }

    public String getChargeUnitPrice() {
        return chargeUnitPrice;
    }

    public void setChargeUnitPrice(String chargeUnitPrice) {
        this.chargeUnitPrice = chargeUnitPrice == null ? null : chargeUnitPrice.trim();
    }
}