package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Data
@Repository
@ApiModel("客服表-实体类")
public class Customer{

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("客服号")
    private String customerId;

    @ApiModelProperty("密码")
    private String customerPwd;

    @ApiModelProperty("客服头像")
    private String customerHeadPortrait;
}
