package com.tib.pojo;

import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@ToString
@Component
public class ChargePile {
    private String id;

    private String cpId;

    private String cpName;

    private String cpCode;

    private String chargeId;

    private String status;

    private String currentV;

    private String currentA;

    private String isDeleted;

    private Date createdTime;

    private Date updatedTime;

    public ChargePile() {
    }

    public ChargePile(String id, String cpId, String cpName, String cpCode, String chargeId, String status, String currentV, String currentA, String isDeleted, Date createdTime, Date updatedTime) {
        this.id = id;
        this.cpId = cpId;
        this.cpName = cpName;
        this.cpCode = cpCode;
        this.chargeId = chargeId;
        this.status = status;
        this.currentV = currentV;
        this.currentA = currentA;
        this.isDeleted = isDeleted;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCpId() {
        return cpId;
    }

    public void setCpId(String cpId) {
        this.cpId = cpId;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public String getCpCode() {
        return cpCode;
    }

    public void setCpCode(String cpCode) {
        this.cpCode = cpCode;
    }

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentV() {
        return currentV;
    }

    public void setCurrentV(String currentV) {
        this.currentV = currentV;
    }

    public String getCurrentA() {
        return currentA;
    }

    public void setCurrentA(String currentA) {
        this.currentA = currentA;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}