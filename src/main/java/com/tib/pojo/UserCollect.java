package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("用户收集表-实体类")
public class UserCollect {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("充电桩id")
    private Integer chargingId;

    @ApiModelProperty("充电桩编号")
    private String chargingPileNum;

    @ApiModelProperty("设备型号")
    private String equipmentType;

    @ApiModelProperty("开放状态")
    private String operateType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getChargingId() {
        return chargingId;
    }

    public void setChargingId(Integer chargingId) {
        this.chargingId = chargingId;
    }

    public String getChargingPileNum() {
        return chargingPileNum;
    }

    public void setChargingPileNum(String chargingPileNum) {
        this.chargingPileNum = chargingPileNum == null ? null : chargingPileNum.trim();
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType == null ? null : equipmentType.trim();
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType == null ? null : operateType.trim();
    }
}