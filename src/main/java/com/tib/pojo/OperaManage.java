package com.tib.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@ToString
@Component
public class OperaManage {
    private String id;

    private String operaId;

    private String operaName;

    private String area;

    private String operaType;

    private String operaIntro;

    private Integer deviceCount;

    private String isDeleted;

    private Date createdTime;

    private Date updatedTime;

    public OperaManage() {
    }

    public OperaManage(String id, String operaId, String operaName, String area, String operaType, String operaIntro, Integer deviceCount, String isDeleted, Date createdTime, Date updatedTime) {
        this.id = id;
        this.operaId = operaId;
        this.operaName = operaName;
        this.area = area;
        this.operaType = operaType;
        this.operaIntro = operaIntro;
        this.deviceCount = deviceCount;
        this.isDeleted = isDeleted;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getOperaId() {
        return operaId;
    }

    public void setOperaId(String operaId) {
        this.operaId = operaId;
    }

    public String getOperaName() {
        return operaName;
    }

    public void setOperaName(String operaName) {
        this.operaName = operaName;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getOperaType() {
        return operaType;
    }

    public void setOperaType(String operaType) {
        this.operaType = operaType;
    }

    public String getOperaIntro() {
        return operaIntro;
    }

    public void setOperaIntro(String operaIntro) {
        this.operaIntro = operaIntro;
    }

    public Integer getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        this.deviceCount = deviceCount;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}