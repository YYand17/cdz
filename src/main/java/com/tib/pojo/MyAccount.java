package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Repository
@ApiModel("我的账户-实体类")
public class MyAccount {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户id")
    private String uId;

    @ApiModelProperty("卡号")
    private String cardNumber;

    @ApiModelProperty("账户余额")
    private Long balance;

    @ApiModelProperty("代金卷总数")
    private Integer vouchersTotal;

    @ApiModelProperty("优惠卷总数")
    private Integer couponTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Integer getVouchersTotal() {
        return vouchersTotal;
    }

    public void setVouchersTotal(Integer vouchersTotal) {
        this.vouchersTotal = vouchersTotal;
    }

    public Integer getCouponTotal() {
        return couponTotal;
    }

    public void setCouponTotal(Integer couponTotal) {
        this.couponTotal = couponTotal;
    }
}