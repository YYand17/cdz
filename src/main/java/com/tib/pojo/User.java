package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("用户表-实体类")
public class User {
    private Integer id;

    private String username;

    private String userpass;

    private String nickname;

    private Integer gender;

    private String registeredProvince;

    private String province;

    private String locationArea;

    private String detailedAddress;

    private String registeredReferrer;

    private String paymentType;

    private String customerType;

    private String customerStatus;

    private String lockCusomerReason;

    private String carNumber;

    private String ownerCertification;

    private String consumerBehavior;

    private Date registeredStartTime;

    private Date registeredEndTime;

    private String email;

    private Long integral;

    private Integer operatorId;

    private String userPicture;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getRegisteredProvince() {
        return registeredProvince;
    }

    public void setRegisteredProvince(String registeredProvince) {
        this.registeredProvince = registeredProvince;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLocationArea() {
        return locationArea;
    }

    public void setLocationArea(String locationArea) {
        this.locationArea = locationArea;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public String getRegisteredReferrer() {
        return registeredReferrer;
    }

    public void setRegisteredReferrer(String registeredReferrer) {
        this.registeredReferrer = registeredReferrer;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getLockCusomerReason() {
        return lockCusomerReason;
    }

    public void setLockCusomerReason(String lockCusomerReason) {
        this.lockCusomerReason = lockCusomerReason;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getOwnerCertification() {
        return ownerCertification;
    }

    public void setOwnerCertification(String ownerCertification) {
        this.ownerCertification = ownerCertification;
    }

    public String getConsumerBehavior() {
        return consumerBehavior;
    }

    public void setConsumerBehavior(String consumerBehavior) {
        this.consumerBehavior = consumerBehavior;
    }

    public Date getRegisteredStartTime() {
        return registeredStartTime;
    }

    public void setRegisteredStartTime(Date registeredStartTime) {
        this.registeredStartTime = registeredStartTime;
    }

    public Date getRegisteredEndTime() {
        return registeredEndTime;
    }

    public void setRegisteredEndTime(Date registeredEndTime) {
        this.registeredEndTime = registeredEndTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIntegral() {
        return integral;
    }

    public void setIntegral(Long integral) {
        this.integral = integral;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }
}
