package com.tib.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("优惠券表-实体类")
public class DiscountCoupon {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户id")
    private String uId;

    @ApiModelProperty("优惠券名称")
    private String dcName;

    @ApiModelProperty("优惠券金额")
    private Long dcMoney;

    @ApiModelProperty("优惠券期限")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dcDate;

    @ApiModelProperty("优惠券使用说明")
    private String dcState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    public Long getDcMoney() {
        return dcMoney;
    }

    public void setDcMoney(Long dcMoney) {
        this.dcMoney = dcMoney;
    }

    public Date getDcDate() {
        return dcDate;
    }

    public void setDcDate(Date dcDate) {
        this.dcDate = dcDate;
    }

    public String getDcState() {
        return dcState;
    }

    public void setDcState(String dcState) {
        this.dcState = dcState;
    }
}