package com.tib.pojo;

import com.tib.utils.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Repository;

@Data
@Repository
@ApiModel("充电桩管理-实体类")
public class Charging {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("充电桩编号")
    private String chargingPileNum;

    @ApiModelProperty("充电桩名称")
    private String chargingPileName;

    @ApiModelProperty("充电桩站点")
    private String chargingPileAddress;

    @ApiModelProperty("充电桩协议")
    private String pileProtocol;

    @ApiModelProperty("设备型号")
    private String equipmentType;

    @ApiModelProperty("开放状态")
    private String openType;

    @ApiModelProperty("运营状态")
    private String operateType;

    @ApiModelProperty("充电前是否检查")
    private String checkType;

    @ApiModelProperty("服务器ip")
    private String serverIp;

    @ApiModelProperty("服务器端口")
    private String serverPort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChargingPileNum() {
        return chargingPileNum;
    }

    public void setChargingPileNum(String chargingPileNum) {
        this.chargingPileNum = chargingPileNum == null ? null : chargingPileNum.trim();
    }

    public String getChargingPileName() {
        return chargingPileName;
    }

    public void setChargingPileName(String chargingPileName) {
        this.chargingPileName = chargingPileName == null ? null : chargingPileName.trim();
    }

    public String getChargingPileAddress() {
        return chargingPileAddress;
    }

    public void setChargingPileAddress(String chargingPileAddress) {
        this.chargingPileAddress = chargingPileAddress == null ? null : chargingPileAddress.trim();
    }

    public String getPileProtocol() {
        return pileProtocol;
    }

    public void setPileProtocol(String pileProtocol) {
        this.pileProtocol = pileProtocol == null ? null : pileProtocol.trim();
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType == null ? null : equipmentType.trim();
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType == null ? null : openType.trim();
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType == null ? null : operateType.trim();
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType == null ? null : checkType.trim();
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp == null ? null : serverIp.trim();
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort == null ? null : serverPort.trim();
    }
}