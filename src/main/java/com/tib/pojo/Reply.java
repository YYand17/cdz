package com.tib.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ApiModel("回复表-实体类")
public class Reply {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("回复id")
    private String rId;

    @ApiModelProperty("评论id")
    private String cId;

    @ApiModelProperty("用户id")
    private String uId;

    @ApiModelProperty("回复被回复的id")
    private String fRId;

    @ApiModelProperty("回复内容")
    private String rComments;

    @ApiModelProperty("回复时间")
    private Date rTime;

    @ApiModelProperty("昵称")
    private String nickname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getfRId() {
        return fRId;
    }

    public void setfRId(String fRId) {
        this.fRId = fRId;
    }

    public String getrComments() {
        return rComments;
    }

    public void setrComments(String rComments) {
        this.rComments = rComments;
    }

    public Date getrTime() {
        return rTime;
    }

    public void setrTime(Date rTime) {
        this.rTime = rTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}