package com.tib.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;

/**
 * @author caoxianghua
 * @create 2022-02-21 13:39
 **/
public class MyTomcatConnectorCustomizer implements TomcatConnectorCustomizer {
    @Override
    public void customize(Connector connector) {
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        // 设置最大连接数
        protocol.setMaxConnections(2000);
        // 设置最大线程数
        protocol.setMaxThreads(2000);
        protocol.setConnectionTimeout(30000);
        // 设置最大缓冲区
        protocol.setMaxHttpHeaderSize(8999);
        protocol.setMaxSavePostSize(4096);
        protocol.setConnectionUploadTimeout(300000);
        protocol.setMaxTrailerSize(8192);
        protocol.setMaxExtensionSize(8192);
        protocol.setMaxSwallowSize(2*1024*1024);

    }
}
