package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.User;
import com.tib.service.UserService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.ResponseResult;
import com.tib.em.StatusConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
@Api(value = "/users",tags = "用户表api")
public class UserController {

    @Autowired
    @Qualifier(value="userServiceImpl")
    private UserService userService;

    @ApiOperation("查询所有信息信息")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        List<Map> resList = userService.select(map);
        for (Map temp : resList){
            if (temp.containsKey("detailed_address")){
                temp.put("detailedAddress",temp.get("detailed_address"));
            }
            if (temp.containsKey("user_picture")){
                temp.put("userPicture",temp.get("user_picture"));
            }
            if (temp.containsKey("userpass")){
                temp.put("password",temp.get("userpass"));
            }
            if(temp.containsKey("gender")){
                if (JSONObject.parseObject(JSON.toJSONString(temp.get("gender")),Integer.class)==1){
                    temp.put("gender","男");
                }
                else if(JSONObject.parseObject(JSON.toJSONString(temp.get("gender")),Integer.class)==2){
                    temp.put("gender","女");
                }
                else {
                    temp.put("gender","未知");
                }
            }
        }
        return JsonResponse.newOk(resList);
    }


    @ApiOperation("查询所有信息信息")
    @PostMapping("/queryAll")
    public List<User> queryAll(){
        System.out.println(userService.queryAll());
        return userService.queryAll();
    }


    @ApiOperation("登录")
    @PostMapping("/login")
    public ResponseResult login(@ApiParam("用户对象") @RequestBody User user){
        return userService.queryIDAndPassword(user);
    }


    @ApiOperation("登出")
    @GetMapping("/logout")
    public ResponseResult logOut(){
        return ResponseResult.getResponseResult(StatusConstants.STATUS_DEFAULT_YES,"退出成功");
    }

    @ApiOperation("注册")
    @PostMapping("/register")
    public ResponseResult register(@RequestBody User user){
        return userService.insertUserInfo(user);
    }

    @ApiOperation("修改")
    @PostMapping("/modify")
    public ResponseResult modify(@ApiParam("用户对象") User user,@ApiParam("旧密码") String oldPassword){
        return userService.updateUserInfo(user,oldPassword);
    }

    @ApiOperation("上传")
    @PostMapping("/upload")
    public ResponseResult uploadImage(@RequestParam("ImageFile") MultipartFile ImageFile,@ApiParam("用户对象") User user){
        return userService.uploadImage(ImageFile,user);
    }

    @ApiOperation("分页查询")
    @GetMapping("/queryPage")
    public PageResult queryPage(@ApiParam("用户对象") User user, @ApiParam("当前页数")Integer page, @ApiParam("行数") Integer row) {
        return userService.queryPage(user,page,row);
    }

@ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        if (map.containsKey("gender")){
            if (JSONObject.parseObject(JSON.toJSONString(map.get("gender")),String.class).equals("男")){
                map.put("gender",1);
            }
            else if (JSONObject.parseObject(JSON.toJSONString(map.get("gender")),String.class).equals("女")){
                map.put("gender",2);
            }
        }
        if (userService.update(map)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }
}
