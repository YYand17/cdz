package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.RecentNews;
import com.tib.service.OperaManageService;
import com.tib.service.RecentNewsService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-14 17:11
 **/

@RestController
@RequestMapping("/operaManage")
@Api(value = "/operaManage",tags = "运营商管理api")
public class OperaManageController {

    @Autowired
    private OperaManageService operaManageService;

    @ApiOperation("删除")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (operaManageService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("增添")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        map.put("id",StringUtil.getGuid32());
        if (operaManageService.insert(map)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("按id查询")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        List<Map> resMap = operaManageService.select(map);
        return JsonResponse.newOk(resMap);
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        if (operaManageService.update(map)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

    @ApiOperation("获取设备数量")
    @PostMapping("/getCount")
    public JsonResponse getCount(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        List<Map> resList = operaManageService.getCount(map);
        Map resMap = new HashMap();
        for (Map temp : resList){
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("腾讯")){
                resMap.put("tx",temp.get("deviceCount"));
            }
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("华为")){
                resMap.put("hw",temp.get("deviceCount"));
            }
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("阿里巴巴")){
                resMap.put("albb",temp.get("deviceCount"));
            }
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("网易")){
                resMap.put("wy",temp.get("deviceCount"));
            }
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("百度")){
                resMap.put("bd",temp.get("deviceCount"));
            }
            if (JSONObject.parseObject(JSON.toJSONString(temp.get("operaName")),String.class).equals("字节跳动")){
                resMap.put("zjtd",temp.get("deviceCount"));
            }
        }
        return JsonResponse.newOk(resMap);
    }
}
