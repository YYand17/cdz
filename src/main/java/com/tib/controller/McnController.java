package com.tib.controller;

import com.tib.pojo.FeeBase;
import com.tib.service.McnService;
import com.tib.utils.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/mcn")
@Api(value = "/mcn",tags = "卡号管理表api")
public class McnController {

    @Autowired
    private McnService mcnService;

    @ApiOperation("查询所有信息")
    @GetMapping("/queryAll")  //查询所有信息
    private List<FeeBase> queryAll() {
        return mcnService.queryAll();
    }

    @ApiOperation("按id查询信息")
    @GetMapping("/selectByPrimaryKey")
    private FeeBase selectByPrimaryKey(@ApiParam("卡号管理表id") Integer id){  // 按编号查询
        return mcnService.selectByPrimaryKey(id);
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    private void insert(@ApiParam("卡号管理表对象") FeeBase feeBase){ //插入管理员信息
        mcnService.insert(feeBase);
    }

    @ApiOperation("有选择性插入信息")
    @PostMapping("/insertSelective")
    private void insertSelective(@ApiParam("卡号管理表对象") FeeBase feeBase){
        mcnService.insertSelective(feeBase);
    }

    @ApiOperation("修改信息")
    @PostMapping("/updateByPrimaryKey")
    private void updateByPrimaryKey(@ApiParam("卡号管理表对象") FeeBase feeBase){ //按主键修改信息
        mcnService.updateByPrimaryKey(feeBase);
    }

    @ApiOperation("有选择性修改信息")
    @PostMapping("/updateByPrimaryKeySelective")
    private void updateByPrimaryKeySelective(@ApiParam("卡号管理表对象") FeeBase feeBase){
        mcnService.updateByPrimaryKeySelective(feeBase);
    }

    @ApiOperation("按id删除信息")
    @PostMapping("/deleteByPrimaryKey")
    private void deleteByPrimaryKey(@ApiParam("卡号管理表id") Integer id){ //通过id删除某个信息
        mcnService.deleteByPrimaryKey(id);
    }

    @ApiOperation("分页查询")
    @GetMapping("/selectPage")
    private PageResult selectPage(@ApiParam("卡号管理对象") FeeBase feeBase, @ApiParam("当前页数")Integer page, @ApiParam("行数") Integer row) {
        return mcnService.selectPage(feeBase,page,row);
    }
}
