package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.Chat;
import com.tib.service.ChatService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/chat")
@Api(value = "/chat",tags = "聊天记录Api")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @ApiOperation("条件查询(含分页)")
    @PostMapping("findAll")
    public JsonResponse findAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(chatService.findAll(map));
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        Chat chat = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Chat.class);
        chat.setId(StringUtil.getGuid32());
        if (chatService.insert(chat)==1)
            return JsonResponse.newOk("添加成功");
        return JsonResponse.newError("添加失败");
    }

    @ApiOperation("删除信息")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (chatService.deleteById(id)==1)
            return JsonResponse.newOk("删除成功");
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Chat chat = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Chat.class);
        if(chatService.updateById(chat)==1)
            return JsonResponse.newOk("修改成功");
        return JsonResponse.newError("修改失败");
    }



}