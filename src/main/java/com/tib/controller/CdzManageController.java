package com.tib.controller;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.service.CdzManageService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-15 15:56
 **/

@RestController
@RequestMapping("/cdzManage")
@Api(value = "/cdzManage",tags = "充电站管理api")
public class CdzManageController {
    @Autowired
    private CdzManageService cdzManageService;

    @ApiOperation("删除")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (cdzManageService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("增添")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        map.put("id", StringUtil.getGuid32());
        if (cdzManageService.insert(map)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("查询")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        List<Map> resMap = cdzManageService.select(map);
        return JsonResponse.newOk(resMap);
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        if (cdzManageService.update(map)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

    @ApiOperation("修改")
    @PostMapping("/selectCsTotal")
    public JsonResponse selectCsTotal(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        List<Map> resMap = cdzManageService.selectCsTotal(map);
        return JsonResponse.newOk(resMap);
    }
}
