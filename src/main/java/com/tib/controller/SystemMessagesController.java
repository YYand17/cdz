package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.SystemMessages;
import com.tib.pojo.User;
import com.tib.service.SystemMessagesService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/systemMessages")
@Api(value = "/systemMessages" ,tags = "系统消息api")
public class SystemMessagesController {
    @Autowired
    SystemMessagesService systemMessagesService;

    @ApiOperation("删除")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (systemMessagesService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("增添")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        SystemMessages record = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),SystemMessages.class);
        record.setId(StringUtil.getGuid32());
        if (systemMessagesService.insert(record)==1){
            return JsonResponse.newOk("添加成功");
        }
        return JsonResponse.newError("添加失败");
    }

    @ApiOperation("查询(含分页)")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(systemMessagesService.select(map));
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        SystemMessages record = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),SystemMessages.class);
        record.setId(StringUtil.getGuid32());
        if (systemMessagesService.update(record)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

}
