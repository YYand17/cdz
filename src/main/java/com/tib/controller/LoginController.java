package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.em.StatusConstants;
import com.tib.service.LoginService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-09 10:27
 **/
@RestController
@RequestMapping("/user")
@Api(value = "/user",tags = "登录")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public JsonResponse login(@RequestBody JsonRequest<?> jsonRequest){
//        Map map = new HashMap();
//        map.put("username","admin");
//        map.put("password","1");
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        if (map.containsKey("password")){
            map.put("userpass",map.get("password"));
        }
        return loginService.login(map);

    }

    @ApiOperation("登出")
    @PostMapping("/logout")
    public JsonResponse logOut(){
        return JsonResponse.newOk("退出成功");
    }




}
