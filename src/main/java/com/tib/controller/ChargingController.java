package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.Charging;
import com.tib.service.ChargingService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/charging")
@Api(value = "/charging",tags = "充电桩管理api")
public class ChargingController {

    @Autowired
    @Qualifier(value="chargingServiceImpl")
    public ChargingService chargingService;

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "充电桩编号",value = "chargingPileNum",paramType = "String"),
            @ApiImplicitParam(name = "充电桩名称",value = "chargingPileName",paramType = "String"),
            @ApiImplicitParam(name = "充电桩站点",value = "chargingPileAddress",paramType = "String"),
            @ApiImplicitParam(name = "充电桩协议",value = "pileProtocol",paramType = "String"),
            @ApiImplicitParam(name = "设备型号",value = "equipmentType",paramType = "String"),
            @ApiImplicitParam(name = "开放状态",value = "openType",paramType = "String"),
            @ApiImplicitParam(name = "运营状态",value = "operateType",paramType = "String"),
            @ApiImplicitParam(name = "充电前是否检查",value = "checkType",paramType = "String"),
            @ApiImplicitParam(name = "服务器ip",value = "serverIp",paramType = "String"),
            @ApiImplicitParam(name = "服务器端口",value = "serverPort",paramType = "String"),
    })
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        Charging charging = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Charging.class);
        charging.setId(StringUtil.getGuid32());
        if (chargingService.insert(charging)==1){
            return JsonResponse.newOk("添加成功");
        }
        return JsonResponse.newError("添加失败");
    }

    @ApiOperation("删除信息")
    @ApiImplicitParam(name = "主键id",value = "id",paramType = "String")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (chargingService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("修改信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "主键id",value = "id",paramType = "String"),
            @ApiImplicitParam(name = "充电桩编号",value = "chargingPileNum",paramType = "String"),
            @ApiImplicitParam(name = "充电桩名称",value = "chargingPileName",paramType = "String"),
            @ApiImplicitParam(name = "充电桩站点",value = "chargingPileAddress",paramType = "String"),
            @ApiImplicitParam(name = "充电桩协议",value = "pileProtocol",paramType = "String"),
            @ApiImplicitParam(name = "设备型号",value = "equipmentType",paramType = "String"),
            @ApiImplicitParam(name = "开放状态",value = "openType",paramType = "String"),
            @ApiImplicitParam(name = "运营状态",value = "operateType",paramType = "String"),
            @ApiImplicitParam(name = "充电前是否检查",value = "checkType",paramType = "String"),
            @ApiImplicitParam(name = "服务器ip",value = "serverIp",paramType = "String"),
            @ApiImplicitParam(name = "服务器端口",value = "serverPort",paramType = "String"),
    })
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Charging charging = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Charging.class);
        if(chargingService.update(charging)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

    @ApiOperation("查询信息通过id(含分页)")
    @ApiImplicitParam(name = "主键id",value = "id",paramType = "String")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        return JsonResponse.newOk(chargingService.select(id));
    }

    @ApiOperation("查询所有信息(含分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "当前页数",value = "pageNo",paramType = "Integer"),
            @ApiImplicitParam(name = "行数",value = "pageSize",paramType = "Integer"),
            @ApiImplicitParam(name = "充电桩编号",value = "chargingPileNum",paramType = "String"),
            @ApiImplicitParam(name = "充电桩名称",value = "chargingPileName",paramType = "String"),
            @ApiImplicitParam(name = "充电桩站点",value = "chargingPileAddress",paramType = "String"),
            @ApiImplicitParam(name = "充电桩协议",value = "pileProtocol",paramType = "String"),
            @ApiImplicitParam(name = "设备型号",value = "equipmentType",paramType = "String"),
            @ApiImplicitParam(name = "开放状态",value = "openType",paramType = "String"),
            @ApiImplicitParam(name = "运营状态",value = "operateType",paramType = "String"),
            @ApiImplicitParam(name = "充电前是否检查",value = "checkType",paramType = "String"),
            @ApiImplicitParam(name = "服务器ip",value = "serverIp",paramType = "String"),
            @ApiImplicitParam(name = "服务器端口",value = "serverPort",paramType = "String"),
    })
    @PostMapping("/queryAll")
    public JsonResponse queryAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(chargingService.queryAll(map));
    }
}
