package com.tib.controller;

import com.tib.pojo.ChargingOrder;
import com.tib.service.ChargingOrderService;
import com.tib.utils.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/chargingOrder")
@Api(value = "/chargingOrder",tags = "充电订单表api")
public class ChargingOrderController {
    @Autowired
    ChargingOrderService chargingOrderService;

    @ApiOperation("按id删除信息")
    @PostMapping("/delete")
    public int delete(@ApiParam("充电订单表id") Integer id){
        return chargingOrderService.delete(id);
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public int insert(@ApiParam("充电订单对象") ChargingOrder record){
        return chargingOrderService.insert(record);
    }

    @ApiOperation("按id查询信息")
    @GetMapping("/selectById")
    public ChargingOrder selectById(@ApiParam("充电订单表id") Integer id){
        return chargingOrderService.selectById(id);
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public int update(@ApiParam("充电订单对象") ChargingOrder record){
        return chargingOrderService.update(record);
    }

    @ApiOperation("按条件查询信息")
    @GetMapping("/selectByBean")
    public List<ChargingOrder> selectByBean(@ApiParam("充电订单对象") ChargingOrder record){
        return chargingOrderService.selectByBean(record);
    }

    @ApiOperation("分页查询")
    @GetMapping("/selectPage")
    public PageResult selectPage(@ApiParam("充电订单对象") ChargingOrder record, @ApiParam("当前页数")Integer page, @ApiParam("行数") Integer row){
        return chargingOrderService.selectPage(record,page,row);
    }

    @ApiOperation("最近充电查询")
    @PostMapping("/selectLately")
    public PageResult selectLately(@ApiParam("充电订单对象") ChargingOrder record, @ApiParam("当前页数")Integer page, @ApiParam("行数") Integer row){
        return chargingOrderService.selectLately(record,page,row);
    }

}
