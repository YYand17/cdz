package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.Charging;
import com.tib.pojo.RechargeRecord;
import com.tib.service.RechargeRecordService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rechargeRecord")
@Api(value = "/rechargeRecord",tags = "充值记录api")
public class RechargeRecordController {

    @Autowired
    RechargeRecordService rechargeRecordService;

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        RechargeRecord rechargeRecord = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),RechargeRecord.class);
        rechargeRecord.setId(StringUtil.getGuid32());
        if (rechargeRecordService.insert(rechargeRecord)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("删除信息")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (rechargeRecordService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        RechargeRecord rechargeRecord = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),RechargeRecord.class);
        if (rechargeRecordService.update(rechargeRecord)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

    @ApiOperation("按表id查询信息")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Map> resMap = rechargeRecordService.select(map);
        for(Map map1:resMap){
            if (map1.containsKey("recharge_time")){
                map1.put("recharge_time",sdf.format(map1.get("recharge_time")));
            }
            if (map1.containsKey("created_time")){
                map1.put("created_time",sdf.format(map1.get("created_time")));
            }
            if (map1.containsKey("updated_time")){
                map1.put("updated_time",sdf.format(map1.get("updated_time")));
            }
        }
        return JsonResponse.newOk(resMap);
    }
}
