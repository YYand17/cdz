package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.ChargingOrder;
import com.tib.pojo.MyAccount;
import com.tib.service.MyAccountService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import java.util.Map;

import static com.tib.em.StatusCodeEnum.SERVER_INTERNAL_ERROR;

@RestController
@RequestMapping("/myAccount")
@Api(value = "/myAccount",tags = "我的账户api")
public class MyAccountController {

    @Autowired
    MyAccountService myAccountService;

    @ApiOperation("删除信息")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (myAccountService.delete(id)==1){
            return JsonResponse.newOk();
        }
        return JsonResponse.newError("删除失败");

    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        MyAccount myAccount = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),MyAccount.class);
        myAccount.setId(StringUtil.getGuid32());
        if (myAccountService.insert(myAccount)==1){
            return JsonResponse.newOk();
        }
        return JsonResponse.newError("插入失败");

    }

    @ApiOperation("查询信息(含分页)")
    @PostMapping("/select")
    public JsonResponse select(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(myAccountService.select(map));
    }

    @ApiOperation("修改(充值，消费，代金券数量修改，优惠券数量修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "卡号",value = "cardNumber"),
            @ApiImplicitParam(name = "修改类型",value = "updatedType"),
            @ApiImplicitParam(name = "金额",value = "balance"),
            @ApiImplicitParam(name = "用户id",value = "uId")
    })
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Map<String,Object> map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        if (myAccountService.update(map)==1){
            return JsonResponse.newOk("success!");
        }
        return JsonResponse.newError(SERVER_INTERNAL_ERROR);
    }
/*

    @ApiOperation("充值")
    @GetMapping("/recharge")
    String recharge(@ApiParam("用户id")Integer uId, @ApiParam("充值金额")float money){
        MyAccount record = new MyAccount();
        record.setuId(uId);
        return myAccountService.recharge(record,money);
    }

    @ApiOperation("消费")
    @GetMapping("/consume")
    String consume(@ApiParam("用户id")Integer uId, @ApiParam("消费金额")float money){
        MyAccount record = new MyAccount();
        record.setuId(uId);
        return myAccountService.consume(record,money);
    }

    //代金券
    @ApiOperation("代金券数量修改")
    @GetMapping("/vouchers")
    String vouchers(@ApiParam("用户id")int uId, @ApiParam("修改数量")int number){
        MyAccount record = new MyAccount();
        record.setuId(uId);
        return myAccountService.vouchersChange(record,number);
    }

    //优惠券
    @ApiOperation("优惠券数量修改")
    @GetMapping("/coupon")
    String coupon(@ApiParam("用户id")int uId, @ApiParam("修改数量")int number){
        MyAccount record = new MyAccount();
        record.setuId(uId);
        return myAccountService.couponChange(record,number);
    }
*/

}
