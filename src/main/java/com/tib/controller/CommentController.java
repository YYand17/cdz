package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.em.StatusCodeEnum;
import com.tib.pojo.Comment;
import com.tib.service.CommentService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.StringUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/comment")
@Api(value = "/comment",tags = "评论表api")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @ApiOperation("查询所有信息(包含分页）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "当前页数",value = "pageNo",paramType = "Integer"),
            @ApiImplicitParam(name = "一页几行",value = "pageSize",paramType = "Integer")
    })
    @PostMapping("/selectAll")
    public JsonResponse selectAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(commentService.selectAll(map));
    }

    @ApiOperation("按id查询信息")
    @ApiImplicitParam(name = "主键id",value = "id",paramType = "String")
    @PostMapping("/selectByPrimaryKey")
    public JsonResponse selectByPrimaryKey(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(commentService.selectByPrimaryKey(map));
    }

    @ApiOperation("插入信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "评论id",value = "cId"),
            @ApiImplicitParam(name = "用户id",value = "uId"),
            @ApiImplicitParam(name = "评论内容",value = "cComments")
    })
    @PostMapping("/insert")
    public JsonResponse insertSelective(@RequestBody JsonRequest<?> jsonRequest){
        Comment comment = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Comment.class);
        comment.setId(StringUtil.getGuid32());
        if (commentService.insertSelective(comment)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError(StatusCodeEnum.SERVER_INTERNAL_ERROR);
    }

    @ApiOperation("修改信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "主键id",value = "id"),
            @ApiImplicitParam(name = "评论id",value = "cId"),
            @ApiImplicitParam(name = "用户id",value = "uId"),
            @ApiImplicitParam(name = "评论内容",value = "cComments")
    })
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Comment comment = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Comment.class);
        if (commentService.update(comment)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError(StatusCodeEnum.SERVER_INTERNAL_ERROR);
    }

    @ApiOperation("删除信息")
    @ApiImplicitParam(name = "主键id",value = "id")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (commentService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError(StatusCodeEnum.SERVER_INTERNAL_ERROR);
    }



}
