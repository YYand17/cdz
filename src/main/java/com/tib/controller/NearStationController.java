package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.NearStation;
import com.tib.service.NearStationService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/nearStation")
@Api(value = "/nearStation",tags = "附近电站表api")
public class NearStationController {

    @Autowired
    private NearStationService nearStationService;

    @ApiOperation("查询所有信息")
    @PostMapping("/queryAll")
    public JsonResponse queryAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(nearStationService.queryAll(map));
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        NearStation nearStation = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),NearStation.class);
        nearStation.setId(StringUtil.getGuid32());
        if(nearStationService.insert(nearStation)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        NearStation nearStation = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),NearStation.class);
        if(nearStationService.update(nearStation)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

    @ApiOperation("按id删除信息")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if(nearStationService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

}
