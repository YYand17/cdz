package com.tib.controller;

import com.tib.pojo.UserCollect;
import com.tib.service.UserCollectService;
import com.tib.utils.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/userCollect")
@Api(value = "/userCollect",tags = "用户收集表api")
public class UserCollectController {
    @Autowired
    UserCollectService userCollectService;

    @ApiOperation("删除信息")
    @PostMapping("/delete")
    public int delete(@ApiParam("用户收集表对象") UserCollect record){
        return userCollectService.delete(record);
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public int insert(@ApiParam("用户收集表对象") UserCollect record){
        return userCollectService.insert(record);
    }

    @ApiOperation("查询某个信息")
    @GetMapping("/selectById")
    public UserCollect selectById(@ApiParam("用户收集表对象") UserCollect record){
        return userCollectService.selectById(record);
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public int updateByPrimaryKeySelective(@ApiParam("用户收集表对象") UserCollect record){
        return userCollectService.updateByPrimaryKeySelective(record);
    }

    @ApiOperation("查询所有信息")
    @GetMapping("/selectByUserId")
    public List<UserCollect> selectByUserId(@ApiParam("用户收集表对象") UserCollect record){
        return userCollectService.selectByUserId(record);
    }

    @ApiOperation("分页查询")
    @GetMapping("/selectPage")
    public PageResult selectPage(@ApiParam("用户收集对象") UserCollect record, @ApiParam("当前页数")Integer page, @ApiParam("行数") Integer row){
        return userCollectService.selectPage(record, page, row);
    }
}
