package com.tib.controller;

import com.tib.pojo.ManagerSite;
import com.tib.service.ManagerSiteService;
import com.tib.utils.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/managerSite")
@Api(value = "/managerSite",tags = "站点管理表api")
public class ManagerSiteController {

    @Autowired
    private ManagerSiteService managerSiteService;

    @ApiOperation("关联表查询")
    @GetMapping("/AssociatedQuery")
    public List<ManagerSite> AssociatedQuery(Integer userId){
        return managerSiteService.AssociatedQuery(userId);
    }

    @ApiOperation("查询所有信息")
    @GetMapping("/queryAll")
    public List<ManagerSite> queryAll(){
        return managerSiteService.queryAll();
    }

    @ApiOperation("按id查询信息")
    @GetMapping("/select")
    public ManagerSite select(@ApiParam("站点管理表id")Integer id){
        return managerSiteService.select(id);
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public int insert(@ApiParam("站点管理表对象")ManagerSite managerSite){
        return managerSiteService.insert(managerSite);
    }

    @ApiOperation("按id修改信息")
    @PostMapping("/update")
    public int update(@ApiParam("站点管理表对象")ManagerSite managerSite){
        return managerSiteService.update(managerSite);
    }

    @ApiOperation("按id删除信息")
    @PostMapping("/delete")
    public int delete(@ApiParam("站点管理表id")Integer id){
        return managerSiteService.delete(id);
    }

    @ApiOperation("分页查询")
    @GetMapping("/selectPage")
    public PageResult selectPage(@ApiParam("站点管理表对象")ManagerSite managerSite,@ApiParam("当前页数")Integer page,@ApiParam("行数")Integer row){
        return managerSiteService.selelctPage(managerSite,page,row);
    }

}
