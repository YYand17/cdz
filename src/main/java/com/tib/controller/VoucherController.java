package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.Voucher;
import com.tib.service.VoucherService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/voucher")
@Api(value = "/voucher",tags = "代金券表api")
public class VoucherController {

    @Autowired
    private VoucherService voucherService;

    @ApiOperation("查询所有信息")
    @PostMapping("/queryAll")
    public JsonResponse queryAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        Integer pageNo = 1;
        Integer pageSize = 10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        return JsonResponse.newOk(voucherService.queryAll(map));
    }

    @ApiOperation("按id查询")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (voucherService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody JsonRequest<?> jsonRequest){
        Voucher record = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Voucher.class);
        record.setId(StringUtil.getGuid32());
        if (voucherService.insert(record)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody JsonRequest<?> jsonRequest){
        Voucher record = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),Voucher.class);
        if (voucherService.update(record)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

}
