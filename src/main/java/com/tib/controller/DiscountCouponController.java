package com.tib.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.pojo.Comment;
import com.tib.pojo.DiscountCoupon;
import com.tib.service.DiscountCouponService;
import com.tib.utils.JsonRequest;
import com.tib.utils.JsonResponse;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/discount")
@Api(value = "/discount",tags = "优惠券表api")
public class DiscountCouponController {

    @Autowired
    private DiscountCouponService discountCouponService;

    @ApiOperation("查询所有信息")
    @PostMapping("/queryAll")
    public JsonResponse queryAll(@RequestBody JsonRequest<?> jsonRequest){
        Map map = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()));
        return JsonResponse.newOk(discountCouponService.queryAll(map));
    }

    @ApiOperation("插入信息")
    @PostMapping("/insert")
    public JsonResponse insert(@RequestBody  JsonRequest<?> jsonRequest){
        DiscountCoupon discountCoupon = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),DiscountCoupon.class);
        discountCoupon.setId(StringUtil.getGuid32());
        if (discountCouponService.insert(discountCoupon)==1){
            return JsonResponse.newOk("插入成功");
        }
        return JsonResponse.newError("插入失败");
    }

    @ApiOperation("按id删除信息")
    @PostMapping("/delete")
    public JsonResponse delete(@RequestBody  JsonRequest<?> jsonRequest){
        String id = jsonRequest.getParamAsString("id");
        if (discountCouponService.delete(id)==1){
            return JsonResponse.newOk("删除成功");
        }
        return JsonResponse.newError("删除失败");
    }

    @ApiOperation("修改信息")
    @PostMapping("/update")
    public JsonResponse update(@RequestBody  JsonRequest<?> jsonRequest){
        DiscountCoupon discountCoupon = JSONObject.parseObject(JSON.toJSONString(jsonRequest.getData()),DiscountCoupon.class);
        if (discountCouponService.update(discountCoupon)==1){
            return JsonResponse.newOk("修改成功");
        }
        return JsonResponse.newError("修改失败");
    }

}
