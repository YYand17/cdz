package com.tib.service;

import com.tib.pojo.Comment;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface CommentService {

    Map<String ,Object> selectByPrimaryKey(Map map);

    Integer insertSelective(Comment comment);

    List<Map<String ,Object>> selectAll(Map map);

    Integer update(Comment comment);

    Integer delete(String id);
}
