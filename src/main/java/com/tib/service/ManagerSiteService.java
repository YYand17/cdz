package com.tib.service;

import com.tib.pojo.ManagerSite;
import com.tib.utils.PageResult;

import java.util.List;

public interface ManagerSiteService {

    List<ManagerSite> queryAll();

    ManagerSite select(Integer id);

    int insert(ManagerSite managerSite);

    int update(ManagerSite managerSite);

    int delete(Integer id);

    PageResult selelctPage(ManagerSite managerSite, Integer page, Integer row);

    List<ManagerSite> AssociatedQuery(Integer userId);
}
