package com.tib.service;

import com.tib.pojo.Charging;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface ChargingService {
    int insert(Charging record);

    int delete(String id);

    int update(Charging charging);

    Map select(String id);

    List<Map<String,Object>> queryAll(Map map);

}
