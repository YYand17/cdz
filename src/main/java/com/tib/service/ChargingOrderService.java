package com.tib.service;

import com.tib.pojo.ChargingOrder;
import com.tib.utils.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ChargingOrderService {
    int delete(Integer id);

    int insert(ChargingOrder record);

    ChargingOrder selectById(Integer id);

    int update(ChargingOrder record);

    List<ChargingOrder> selectByBean(ChargingOrder record);

    PageResult selectPage(ChargingOrder record, Integer page, Integer row);

    PageResult selectLately(ChargingOrder record, Integer page, Integer row);
}
