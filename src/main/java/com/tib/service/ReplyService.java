package com.tib.service;

import com.tib.pojo.Reply;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface ReplyService {
    int delete(String id);

    int insert(Reply record);

    int update(Reply record);

    List<Map> select(Map map);
}
