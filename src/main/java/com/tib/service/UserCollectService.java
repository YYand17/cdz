package com.tib.service;

import com.tib.pojo.UserCollect;
import com.tib.utils.PageResult;

import java.util.List;

public interface UserCollectService {

    int delete(UserCollect record);

    int insert(UserCollect record);

    UserCollect selectById(UserCollect record);

    int updateByPrimaryKeySelective(UserCollect record);

    List<UserCollect> selectByUserId(UserCollect record);

    PageResult selectPage(UserCollect record, Integer page, Integer row);
}
