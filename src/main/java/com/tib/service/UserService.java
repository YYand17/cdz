package com.tib.service;

import com.tib.pojo.User;
import com.tib.utils.PageResult;
import com.tib.utils.ResponseResult;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UserService {

    //获取所有人的信息
    List<User> queryAll();

    //通过ID获取用户信息
    User queryUserByID(Integer id);

    ResponseResult queryIDAndPassword(User user);

    ResponseResult insertUserInfo(User user);

    ResponseResult updateUserInfo(User user,String oldPassword);

    ResponseResult uploadImage(MultipartFile file,User user);

    PageResult queryPage(User user, int page, int row);

    List<Map> select(Map map);

    int update(Map map);
}
