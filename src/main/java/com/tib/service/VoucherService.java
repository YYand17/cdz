package com.tib.service;

import com.tib.pojo.Voucher;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface VoucherService {
    List<Map> queryAll(Map map);

    int delete(String id);

    int insert(Voucher record);

    int update(Voucher record);
}
