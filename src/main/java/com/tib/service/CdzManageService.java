package com.tib.service;

import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-15 15:57
 **/
public interface CdzManageService {
    int delete(String id);

    int insert(Map map);

    List<Map> select(Map map);

    int update(Map map);

    List<Map> selectCsTotal(Map map);
}
