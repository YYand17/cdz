package com.tib.service;

import com.tib.utils.JsonResponse;

import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-09 16:00
 **/
public interface LoginService {

    JsonResponse login(Map<String,Object> map);


}
