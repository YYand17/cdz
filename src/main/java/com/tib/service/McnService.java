package com.tib.service;

import com.tib.pojo.FeeBase;
import com.tib.utils.PageResult;

import java.util.List;

public interface McnService {

    //获取管理员所有信息
    List<FeeBase> queryAll();

    FeeBase selectByPrimaryKey(Integer id);

    void insert(FeeBase feeBase);

    void insertSelective(FeeBase feeBase);

    void updateByPrimaryKey(FeeBase feeBase);

    void updateByPrimaryKeySelective(FeeBase feeBase);

    void deleteByPrimaryKey(Integer id);

    PageResult selectPage(FeeBase feeBase, Integer page, Integer row);
}
