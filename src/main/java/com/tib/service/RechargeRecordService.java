package com.tib.service;

import com.tib.pojo.RechargeRecord;
import com.tib.utils.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RechargeRecordService {
    int delete(String id);

    int insert(RechargeRecord record);

    List<Map> select(Map map);

    int update(RechargeRecord record);

}
