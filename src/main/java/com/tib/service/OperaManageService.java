package com.tib.service;

import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-14 17:12
 **/
public interface OperaManageService {
    List<Map> select(Map map);

    int insert(Map map);

    int update(Map map);

    int delete(String id);

    List<Map> getCount(Map map);
}
