package com.tib.service;

import com.tib.pojo.FeedBack;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface FeedBackService {
    List<Map> select(Map map);

    int insert(FeedBack feedBack);

    int delete(String id);

    int update(FeedBack feedBack);
}
