package com.tib.service;

import com.tib.pojo.SystemMessages;
import com.tib.utils.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SystemMessagesService {
    int delete(String id);

    int insert(SystemMessages record);

    List<Map> select(Map map);

    int update(SystemMessages record);

}
