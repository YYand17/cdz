package com.tib.service.impl;

import com.tib.dao.ChargingMapper;
import com.tib.pojo.Charging;
import com.tib.service.ChargingService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChargingServiceImpl implements ChargingService {
    @Autowired
    private ChargingMapper chargingMapper;

    public int insert(Charging record){ return chargingMapper.insert(record);}

    public int delete(String id){ return chargingMapper.delete(id);}

    public int update(Charging charging){
        return chargingMapper.update(charging);
    }

    public Map select(String id){ return chargingMapper.select(id);}

    public List<Map<String,Object>> queryAll(Map map){
        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        List<Map<String,Object>> resList = new ArrayList<>();
        resList.add(chargingMapper.selectCount());
        resList.addAll(chargingMapper.queryAll(map));
        return resList;
    }


}
