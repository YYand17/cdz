package com.tib.service.impl;

import com.tib.dao.FeeBaseMapper;
import com.tib.pojo.FeeBase;
import com.tib.service.McnService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class McnServiceImpl implements McnService {

    @Autowired
    private FeeBaseMapper feeBaseMapper ;

    @Override
    public List<FeeBase> queryAll() {
        return feeBaseMapper.queryAll();
    }

    @Override
    public FeeBase selectByPrimaryKey(Integer id) {
        return feeBaseMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insert(FeeBase feeBase) {
        feeBaseMapper.insert(feeBase);
    }

    @Override
    public void insertSelective(FeeBase feeBase) {
        feeBaseMapper.insertSelective(feeBase);
    }

    @Override
    public void updateByPrimaryKey(FeeBase feeBase) {
        feeBaseMapper.updateByPrimaryKey(feeBase);
    }

    @Override
    public void updateByPrimaryKeySelective(FeeBase feeBase) {
        feeBaseMapper.updateByPrimaryKeySelective(feeBase);
    }

    @Override
    public void deleteByPrimaryKey(Integer id) {
        feeBaseMapper.deleteByPrimaryKey(id);
    }

    @Override
    public PageResult selectPage(FeeBase feeBase, Integer page, Integer row) {
        page = page == null ? PageResult.DEFAULT_PAGE : page;
        row = row == null ? PageResult.DEFAULT_ROW : row;
        List<FeeBase> data = feeBaseMapper.selectPage(feeBase, (page-1)*row, row);
        long total = feeBaseMapper.selectPageCount(feeBase);
        long totalPage = total%row == 0 ? total/row : total/row+1;
        PageResult result = new PageResult(total, totalPage, data);
        return result;
    }
}
