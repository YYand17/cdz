package com.tib.service.impl;

import com.tib.dao.NearStationMapper;
import com.tib.utils.PageResult;
import com.tib.pojo.NearStation;
import com.tib.service.NearStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class NearStationServiceImpl implements NearStationService {

    @Autowired
    private NearStationMapper nearStationMapper;

    @Override
    public int insert(NearStation nearStation) {
        return nearStationMapper.insert(nearStation);
    }

    @Override
    public int update(NearStation nearStation) {
        return nearStationMapper.update(nearStation);
    }

    @Override
    public int delete(String id) {
        return nearStationMapper.delete(id);
    }

    @Override
    public List<Map> queryAll(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")?(Integer) map.get("pageNo"):1;
        Integer pageSize = map.containsKey("pageSize")?(Integer) map.get("pageSize"):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(nearStationMapper.selectCount(map));
        resList.addAll(nearStationMapper.queryAll(map));
        return resList;
    }

}
