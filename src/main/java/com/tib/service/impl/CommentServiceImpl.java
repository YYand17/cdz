package com.tib.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tib.dao.CommentMapper;
import com.tib.pojo.Comment;
import com.tib.service.CommentService;
import com.tib.utils.PageResult;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;


    @Override
    public Map<String, Object> selectByPrimaryKey(Map map) {
        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        return commentMapper.selectByPrimaryKey(map);
    }

    @Override
    public Integer insertSelective(Comment comment) {
        return commentMapper.insertSelective(comment);
    }

    @Override
    public List<Map<String,Object>> selectAll(Map map) {
        List<Map<String ,Object>> resList = new ArrayList<>();
        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(commentMapper.selectCount());
        resList.addAll(commentMapper.selectAll(map));
        return resList;
    }

    @Override
    public Integer update(Comment comment) {
        return commentMapper.update(comment);
    }

    @Override
    public Integer delete(String id) {
        return commentMapper.delete(id);
    }

}
