package com.tib.service.impl;

import com.tib.dao.ChatMapper;
import com.tib.pojo.Chat;
import com.tib.service.ChatService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private ChatMapper chatMapper;

    @Override
    public int deleteById(String id) {
        return chatMapper.deleteById(id);
    }

    @Override
    public int insert(Chat record) {
        return chatMapper.insert(record);
    }

    @Override
    public int updateById(Chat record) {
        return chatMapper.updateById(record);
    }

    @Override
    public List<Map<String,Object>> findAll(Map map) {
        List<Map<String,Object>> resList = new ArrayList<>();
        Integer pageNo =  map.containsKey("pageNo")?(Integer)map.get("pageNo"):1;
        Integer pageSize = map.containsKey("pageSize")?(Integer)map.get("pageSize"):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(chatMapper.selectCount());
        resList.addAll(chatMapper.findAll(map));
        return resList;
    }

}