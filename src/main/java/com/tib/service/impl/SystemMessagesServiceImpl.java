package com.tib.service.impl;

import com.tib.dao.SystemMessagesMapper;
import com.tib.pojo.SystemMessages;
import com.tib.service.SystemMessagesService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SystemMessagesServiceImpl implements SystemMessagesService {

    @Autowired
    SystemMessagesMapper systemMessagesMapper;
    @Override
    public int delete(String id) {
        return systemMessagesMapper.delete(id);
    }

    @Override
    public int insert(SystemMessages record) {
        return systemMessagesMapper.insert(record);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? (Integer) map.get("pageNo") :1;
        Integer pageSize = map.containsKey("pageSize")? (Integer) map.get("pageSize") :10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(systemMessagesMapper.selectCount(map));
        resList.addAll(systemMessagesMapper.select(map));
        return resList;
    }

    @Override
    public int update(SystemMessages record) {
        return systemMessagesMapper.update(record);
    }


}
