package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.OperaManageMapper;
import com.tib.service.OperaManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-14 17:12
 **/

@Service
public class OperaManageServiceImpl implements OperaManageService {

    @Autowired
    private OperaManageMapper operaManageMapper;

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class):1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        int total = operaManageMapper.selectCount(map);
        List<Map> tempMap = operaManageMapper.select(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }

    @Override
    public int insert(Map map) {
        return operaManageMapper.insert(map);
    }

    @Override
    public int update(Map map) {
        return operaManageMapper.update(map);
    }

    @Override
    public int delete(String id) {
        return operaManageMapper.delete(id);
    }

    @Override
    public List<Map> getCount(Map map) {
        return operaManageMapper.getCount(map);
    }
}
