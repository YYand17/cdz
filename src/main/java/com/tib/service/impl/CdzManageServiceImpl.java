package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.CdzManageMapper;
import com.tib.service.CdzManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-15 15:57
 **/
@Service
public class CdzManageServiceImpl implements CdzManageService {

    @Autowired
    private CdzManageMapper cdzManageMapper;

    @Override
    public int delete(String id) {
        return cdzManageMapper.delete(id);
    }

    @Override
    public int insert(Map map) {
        return cdzManageMapper.insert(map);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class):1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        int total = cdzManageMapper.selectCount(map);
        List<Map> tempMap = cdzManageMapper.select(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }

    @Override
    public int update(Map map) {
        return cdzManageMapper.update(map);
    }

    @Override
    public List<Map> selectCsTotal(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class):1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        int total = cdzManageMapper.selectCsTotalCount(map);
        List<Map> tempMap = cdzManageMapper.selectCsTotal(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }
}
