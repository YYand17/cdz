package com.tib.service.impl;

import com.tib.dao.UserCollectMapper;
import com.tib.pojo.UserCollect;
import com.tib.service.UserCollectService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCollectServiceImpl implements UserCollectService {

    @Autowired
    UserCollectMapper userCollectMapper;
    @Override
    public int delete(UserCollect record) {
        return userCollectMapper.deleteByPrimaryKey(record.getId());
    }

    @Override
    public int insert(UserCollect record) {
        return userCollectMapper.insert(record);
    }

    @Override
    public UserCollect selectById(UserCollect record) {
        return userCollectMapper.selectByPrimaryKey(record.getId());
    }

    @Override
    public int updateByPrimaryKeySelective(UserCollect record) {
        return userCollectMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<UserCollect> selectByUserId(UserCollect record) {
        return userCollectMapper.selectByUserId(record);
    }

    @Override
    public PageResult selectPage(UserCollect record, Integer page, Integer row) {
        page = page == null ? PageResult.DEFAULT_PAGE : page;
        row = row == null ? PageResult.DEFAULT_ROW : row;
        List<UserCollect> data = userCollectMapper.selectPage(record, (page-1)*row, row);
        long total = userCollectMapper.selectPageCount(record);
        long totalPage = total%row == 0 ? total/row : total/row+1;
        PageResult result = new PageResult(total, totalPage, data);
        return result;
    }
}
