package com.tib.service.impl;

import com.tib.dao.RechargeRecordMapper;
import com.tib.pojo.Charging;
import com.tib.pojo.RechargeRecord;
import com.tib.service.RechargeRecordService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RechargeRecordServiceImpl implements RechargeRecordService {

    @Autowired
    RechargeRecordMapper rechargeRecordMapper;

    @Override
    public int delete(String id) {
        return rechargeRecordMapper.delete(id);
    }

    @Override
    public int insert(RechargeRecord record) {
        return rechargeRecordMapper.insert(record);
    }

    @Override
    public int update(RechargeRecord record) {
        return rechargeRecordMapper.update(record);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? (Integer) map.get("pageNo") :1;
        Integer pageSize = map.containsKey("pageSize")? (Integer) map.get("pageSize") :10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(rechargeRecordMapper.selectCount(map));
        resList.addAll(rechargeRecordMapper.select(map));
        return resList;
    }
}
