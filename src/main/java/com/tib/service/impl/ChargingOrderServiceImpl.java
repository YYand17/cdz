package com.tib.service.impl;

import com.tib.dao.ChargingOrderMapper;
import com.tib.pojo.ChargingOrder;
import com.tib.service.ChargingOrderService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ChargingOrderServiceImpl implements ChargingOrderService {
    @Autowired
    ChargingOrderMapper chargingOrderMapper;
    @Override
    public int delete(Integer id) {
        return chargingOrderMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ChargingOrder record) {
        return chargingOrderMapper.insertSelective(record);
    }

    @Override
    public ChargingOrder selectById(Integer id) {
        return selectById(id);
    }

    @Override
    public int update(ChargingOrder record) {
        return chargingOrderMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<ChargingOrder> selectByBean(ChargingOrder record) {
        return chargingOrderMapper.selectByBean(record);
    }

    @Override
    public PageResult selectPage(ChargingOrder record, Integer page, Integer row) {
        page = page == null ? PageResult.DEFAULT_PAGE : page;
        row = row == null ? PageResult.DEFAULT_ROW : row;
        List<ChargingOrder> data = chargingOrderMapper.selectPage(record, (page-1)*row, row);
        long total = data.size();
        long totalPage = total%row == 0 ? total/row : total/row+1;
        PageResult result = new PageResult(total, totalPage, data);
        return result;
    }

    @Override
    public PageResult selectLately(ChargingOrder record, Integer page, Integer row) {
        page = page == null ? PageResult.DEFAULT_PAGE : page;
        row = row == null ? PageResult.DEFAULT_ROW : row;
        List<ChargingOrder> data = chargingOrderMapper.selectLately(record, (page-1)*row, row);
        long total = chargingOrderMapper.selectPageCount(record);
        long totalPage = total%row == 0 ? total/row : total/row+1;
        PageResult result = new PageResult(total, totalPage, data);
        return result;
    }
}
