package com.tib.service.impl;

import com.tib.dao.DiscountCouponMapper;
import com.tib.pojo.DiscountCoupon;
import com.tib.service.DiscountCouponService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DiscountCouponServiceImpl implements DiscountCouponService {

    @Autowired
    private DiscountCouponMapper discountCouponMapper;

    @Override
    public List<Map> queryAll(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? (Integer) map.get("pageNo") :1;
        Integer pageSize = map.containsKey("pageSize")? (Integer) map.get("pageSize") :10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(discountCouponMapper.selectCount());
        resList.addAll(discountCouponMapper.queryAll(map));
        return resList;
    }

    @Override
    public int insert(DiscountCoupon record) {
        return discountCouponMapper.insert(record);
    }

    @Override
    public int delete(String id) {
        return discountCouponMapper.delete(id);
    }

    @Override
    public int update(DiscountCoupon record) {
        return discountCouponMapper.update(record);
    }

}
