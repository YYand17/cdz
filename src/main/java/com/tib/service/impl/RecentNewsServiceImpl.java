package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.RecentNewsMapper;
import com.tib.pojo.FeeBase;
import com.tib.pojo.RecentNews;
import com.tib.service.RecentNewsService;
import com.tib.utils.PageResult;
import com.tib.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RecentNewsServiceImpl implements RecentNewsService {

    @Autowired
    RecentNewsMapper recentNewsMapper;
    @Override
    public int delete(String id) {
        return recentNewsMapper.delete(id);
    }

    @Override
    public int insert(RecentNews record) {
        return recentNewsMapper.insert(record);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class) :1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class) :10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        Integer total = recentNewsMapper.selectCount(map);
        List<Map> tempMap = recentNewsMapper.select(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }

    @Override
    public int update(Map map) {
        return recentNewsMapper.update(map);
    }

}
