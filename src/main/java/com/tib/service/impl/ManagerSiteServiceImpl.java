package com.tib.service.impl;

import com.tib.dao.ManagerSiteMapper;
import com.tib.pojo.ManagerSite;
import com.tib.service.ManagerSiteService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ManagerSiteServiceImpl implements ManagerSiteService {

    @Autowired
    private ManagerSiteMapper managerSiteMapper;

    @Override
    public List<ManagerSite> queryAll() {
        return managerSiteMapper.queryAll();
    }

    @Override
    public ManagerSite select(Integer id) {
        return managerSiteMapper.select(id);
    }

    @Override
    public int insert(ManagerSite managerSite) {
        return managerSiteMapper.insert(managerSite);
    }

    @Override
    public int update(ManagerSite managerSite) {
        return managerSiteMapper.update(managerSite);
    }

    @Override
    public int delete(Integer id) {
        return managerSiteMapper.delete(id);
    }

    @Override
    public PageResult selelctPage(ManagerSite managerSite, Integer page, Integer row) {
        page = page==null?PageResult.DEFAULT_PAGE:page;
        row = row==null?PageResult.DEFAULT_ROW:row;
        List<ManagerSite> data = managerSiteMapper.selectPage(managerSite,page,row);
        long total = managerSiteMapper.selectPageCount();
        long totalPage = total%row==0?total/row:total/row+1;
        PageResult result = new PageResult(total,totalPage,data);
        return result;
    }

    @Override
    public List<ManagerSite> AssociatedQuery(Integer userId) {
        return managerSiteMapper.AssociatedQuery(userId);
    }
}
