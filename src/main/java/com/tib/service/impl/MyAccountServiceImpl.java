package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.MyAccountMapper;
import com.tib.pojo.Charging;
import com.tib.pojo.MyAccount;
import com.tib.service.MyAccountService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MyAccountServiceImpl implements MyAccountService {

    @Autowired
    MyAccountMapper myAccountMapper;

    @Autowired
    MyAccountService myAccountService;

    @Override
    public int delete(String id) {
        return myAccountMapper.delete(id);
    }

    @Override
    public int insert(MyAccount record) {
        return myAccountMapper.insert(record);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")?(Integer) map.get("pageNo"):1;
        Integer pageSize = map.containsKey("pageSize")?(Integer) map.get("pageSize"):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(myAccountMapper.selectCount(map));
        resList.addAll(myAccountMapper.select(map));
        return resList;
    }
/*
    @Override
    public String recharge(MyAccount record, float money) {
        Float m = myAccountMapper.selectPage(record,0,1).get(0).getBalance();
        record.setBalance(m + money);
        myAccountMapper.updateByPrimaryKeySelective(record);
        return "充值成功";
    }

    @Override
    public String consume(MyAccount record, float money) {
        Float m = myAccountMapper.selectPage(record,0,1).get(0).getBalance();
        if(money > m){
            return "余额不足";
        }
        record.setBalance(m - money);
        myAccountMapper.updateByPrimaryKeySelective(record);
        return "扣费成功";
    }

    @Override
    public String vouchersChange(MyAccount record, int number) {
        int m = myAccountMapper.selectPage(record,0,1).get(0).getVouchersTotal();
        int num = m + number;
        if(num < 0){
            return "代金券不足";
        }
        record.setVouchersTotal(num);
        myAccountMapper.updateByPrimaryKeySelective(record);
        return "成功";
    }

    @Override
    public String couponChange(MyAccount record, int number) {
        int m = myAccountMapper.selectPage(record,0,1).get(0).getCouponTotal();
        int num = m + number;
        if(num < 0){
            return "优惠券不足";
        }
        record.setCouponTotal(num);
        myAccountMapper.updateByPrimaryKeySelective(record);
        return "成功";
    }*/


    @Override
    public int update(Map<String,Object> map) {
        if (map.get("updatedType").equals("01")){ //充值
            Map<String,Object> selectMap = myAccountMapper.selected(map);
            BigDecimal balance = JSONObject.parseObject(JSON.toJSONString(selectMap.get("balance")),BigDecimal.class);
            BigDecimal Balance = JSONObject.parseObject(JSON.toJSONString(map.get("balance")),BigDecimal.class);
            map.put("balance",balance.add(Balance));
        }
        if (map.get("updatedType").equals("02")) { //消费
            Map<String,Object> selectMap = myAccountMapper.selected(map);
            BigDecimal balance = JSONObject.parseObject(JSON.toJSONString(selectMap.get("balance")),BigDecimal.class);
            BigDecimal Balance = JSONObject.parseObject(JSON.toJSONString(map.get("balance")),BigDecimal.class);
            if(balance.compareTo(Balance)==-1)
            {
                return 0;
            }
            map.put("balance",balance.subtract(Balance));
        }
        return myAccountMapper.update(map);
    }
}