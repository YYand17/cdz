package com.tib.service.impl;

import com.tib.dao.FeedBackMapper;
import com.tib.pojo.FeedBack;
import com.tib.service.FeedBackService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class FeedBackServiceImpl implements FeedBackService {

    @Autowired
    private FeedBackMapper feedBackMapper;

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? (Integer) map.get("pageNO") :1;
        Integer pageSize = map.containsKey("pageSize")? (Integer) map.get("pageSize") :10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        resList.add(feedBackMapper.selectCount());
        resList.addAll(feedBackMapper.select(map));
        return resList;
    }

    @Override
    public int insert(FeedBack feedBack) {
        return feedBackMapper.insert(feedBack);
    }

    @Override
    public int update(FeedBack feedBack) {
        return feedBackMapper.update(feedBack);
    }


    @Override
    public int delete(String id) {
        return feedBackMapper.delete(id);
    }


}
