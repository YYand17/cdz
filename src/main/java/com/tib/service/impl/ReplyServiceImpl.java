package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.ReplyMapper;
import com.tib.pojo.Reply;
import com.tib.service.ReplyService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ReplyServiceImpl implements ReplyService {

    @Autowired
    private  ReplyMapper replyMapper;

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class) : 1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class) : 10;
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        resList.add(replyMapper.selectCount(map));
        resList.addAll(replyMapper.select(map));
        return resList;
    }

    @Override
    public int insert(Reply reply) {
        return replyMapper.insert(reply);
    }

    @Override
    public int update(Reply reply) {
        return replyMapper.update(reply);
    }

    @Override
    public int delete(String id) {
        return replyMapper.delete(id);
    }

}
