package com.tib.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tib.dao.ChargePileMapper;
import com.tib.service.ChargePileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-15 14:35
 **/

@Service
public class ChargePileServiceImpl implements ChargePileService {

    @Autowired
    private ChargePileMapper chargePileMapper;

    @Override
    public int delete(String id) {
        return chargePileMapper.delete(id);
    }

    @Override
    public int insert(Map map) {
        return chargePileMapper.insert(map);
    }

    @Override
    public List<Map> select(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class):1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        int total = chargePileMapper.selectCount(map);
        List<Map> tempMap = chargePileMapper.select(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }

    @Override
    public int update(Map map) {
        return chargePileMapper.update(map);
    }

    @Override
    public List<Map> selectCpTotal(Map map) {
        List<Map> resList = new ArrayList<>();
        Integer pageNo = map.containsKey("pageNo")? JSONObject.parseObject(JSON.toJSONString(map.get("pageNo")),Integer.class):1;
        Integer pageSize = map.containsKey("pageSize")? JSONObject.parseObject(JSON.toJSONString(map.get("pageSize")),Integer.class):10;
        map.put("pageNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        int total = chargePileMapper.selectCpTotalCount(map);
        List<Map> tempMap = chargePileMapper.selectCpTotal(map);
        for (Map temp:tempMap){
            temp.put("total",total);
        }
        resList.addAll(tempMap);
        return resList;
    }
}
