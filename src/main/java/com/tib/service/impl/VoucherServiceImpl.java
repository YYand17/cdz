package com.tib.service.impl;

import com.tib.dao.VoucherMapper;
import com.tib.pojo.Voucher;
import com.tib.service.VoucherService;
import com.tib.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class VoucherServiceImpl implements VoucherService {

    @Autowired
    private VoucherMapper voucherMapper;

    @Override
    public List<Map> queryAll(Map map) {
        List<Map> resMap = new ArrayList<>();
        resMap.add(voucherMapper.selectCount(map));
        resMap.addAll(voucherMapper.queryAll(map));
        return resMap;
    }

    @Override
    public int delete(String id) {
        return voucherMapper.delete(id);
    }

    @Override
    public int insert(Voucher record) {
        return voucherMapper.insert(record);
    }

    @Override
    public int update(Voucher record) {
        return voucherMapper.update(record);
    }


}
