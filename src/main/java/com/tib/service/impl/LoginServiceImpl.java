package com.tib.service.impl;

import com.tib.dao.LoginMapper;
import com.tib.dao.UserMapper;
import com.tib.em.StatusConstants;
import com.tib.pojo.User;
import com.tib.service.LoginService;
import com.tib.utils.JsonResponse;
import com.tib.utils.JwtUtil;
import com.tib.utils.ResponseResult;
import com.tib.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author caoxianghua
 * @create 2022-02-09 16:00
 **/

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public JsonResponse login(Map<String,Object> map) {
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        User user = new User();
        user.setUsername(username);
        user.setUserpass(password);
        User userTemp = userMapper.queryIDAndPassword(user);
        if(userTemp!=null){
            Map<String,Object> userMap = new HashMap<>();
            userMap.put("username",userTemp.getUsername());
            userMap.put("password",userTemp.getUserpass());
            //1、返回结果的map
            Map<String,Object> result = new HashMap<>();
            result.put("userMap",userMap);
            //2、给予token的负载
            HashMap<String,Object> tokenMap = new HashMap<>();
            tokenMap.put("username",userMap.get("username"));
            tokenMap.put("password",userMap.get("password"));
            try {
                //创建token
                result.put("Admin-Token", JwtUtil.createToken(tokenMap));
                return JsonResponse.newOk(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return JsonResponse.newError("登录失败");
    }
}
