package com.tib.service;

import com.tib.pojo.NearStation;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface NearStationService {

    int insert(NearStation nearStation);

    int update(NearStation nearStation);

    int delete(String id);

    List<Map> queryAll(Map map);
}
