package com.tib.service;

import com.tib.pojo.Chat;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface ChatService {

    int deleteById(String id);

    int insert(Chat record);

    int updateById(Chat record);

    List<Map<String,Object>> findAll(Map map);

}
