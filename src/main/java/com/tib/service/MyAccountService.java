package com.tib.service;

import com.tib.pojo.MyAccount;
import com.tib.utils.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MyAccountService {

    int delete(String id);

    int insert(MyAccount record);

    List<Map> select(Map map);

   /* String recharge(MyAccount record, float money);

    String consume(MyAccount record, float money);

    //代金券
    String vouchersChange(MyAccount record, int number);

    //优惠券
    String couponChange(MyAccount record, int number);*/

    int update(Map<String,Object> map);
}
