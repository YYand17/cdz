package com.tib.service;

import com.tib.pojo.RecentNews;
import com.tib.utils.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RecentNewsService {
    int delete(String id);

    int insert(RecentNews record);

    List<Map> select(Map map);

    int update(Map map);

}
