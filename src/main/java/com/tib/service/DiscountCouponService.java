package com.tib.service;

import com.tib.pojo.DiscountCoupon;
import com.tib.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface DiscountCouponService {
    List<Map> queryAll(Map map);

    int insert(DiscountCoupon record);

    int delete(String id);

    int update(DiscountCoupon record);

}
